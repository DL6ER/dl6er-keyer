#include "routines.h"

unsigned long pttTimer = 0;

// Activate transmitter and/or sidetone
void tx()
{
  if(!pttActive && !TRXMute && key_trx)
  {
    ptt_on();
    if(pttLeadInTime && !waiting_for_PTT)
    { // LeadInTime is given in units of 10msec
      pttTimer = micros();
      waiting_for_PTT = TRUE;
      return;
    }
  }
  if(waiting_for_PTT && (micros() - pttTimer) < ((unsigned long)10000*pttLeadInTime))
  {
    // Still have to wait for the PTT to come up
    time = micros();
    return;
  }
  waiting_for_PTT = FALSE;
  key_on();
  // Do this in order to avoid automatic droping of PTT if LeadIn time was high
  keyOffTime = micros();
}

// Deactivate transmitter and sidetone
void rx()
{
  key_off();
}

void key_on()
{
  keyActive = TRUE;

  if(!TRXMute && key_trx && key_output == 1)
    digitalWrite(CONN_KEY1, TRXActive);
  else if(!TRXMute && key_trx && key_output == 2)
    digitalWrite(CONN_KEY2, TRXActive);

  if(sidetoneActive || config)
    tone(CONN_SPEAKER, sidetoneFreq+sidetoneShift);
}

void key_off()
{
  keyActive = FALSE;

  if(key_output == 1)
    digitalWrite(CONN_KEY1, !TRXActive);
  else if(key_output == 2)
    digitalWrite(CONN_KEY2, !TRXActive);
  if(sidetoneActive || config)
    noTone(CONN_SPEAKER);

  keyOffTime = micros();
}

void ptt_on()
{
  pttActive = TRUE;

  if(!key_trx) return;

  if(key_output == 1)
    digitalWrite(CONN_PTT1, HIGH);
  else if(key_output == 2)
    digitalWrite(CONN_PTT2, HIGH);
}

void ptt_off()
{
  pttActive = FALSE;

  if(key_output == 1)
    digitalWrite(CONN_PTT1, LOW);
  else if(key_output == 2)
    digitalWrite(CONN_PTT2, LOW);
}
