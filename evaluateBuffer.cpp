#include "routines.h"

byte WinkeyPause = 0;
bool ProSign = FALSE;

// Send text which is stored in the sendbuffer (already coded for sending)
void evaluateBuffer()
{
  char skip = FALSE;

  if(sendbuffer[0] == 'E')
  { // Send dit
    // Actually key TRX only if sending memory text
    if((sendMem || sendWinkey) && configFlag != recMem)
      key_trx = TRUE;
    else
      key_trx = FALSE;

    tx();
    time = micros();
    state = DIT;
    sendBuffer = TRUE;
  }
  else if(sendbuffer[0] == 'T')
  { // Send dah
    // Actually key TRX only if sending memory text
    if((sendMem || sendWinkey) && configFlag != recMem)
      key_trx = TRUE;
    else
      key_trx = FALSE;

    tx();
    time = micros();
    state = DAH;
    sendBuffer = TRUE;
  }
  else if(sendbuffer[0] == ' ')
  { // Send pause
    time = micros();
    state = INTRALETTERPAUSE;
    sendBuffer = TRUE;
  }
  else if(sendbuffer[0] == 'H')
  { // Send 1/2 pause (needed by the Winkey implementation)
    // Reduce time to the next event by 1/2 ditlength (to simulate a 1/2 pause)
    time = micros() - ditlength/2;
    state = INTRALETTERPAUSE;
    sendBuffer = TRUE;
  }
  else if(sendbuffer[0] == 0x01)
  { // Change sidetone by -100Hz
    sidetoneShift = -100;
  }
  else if(sendbuffer[0] == 0x02)
  { // Reset sidetone shift
    sidetoneShift = 0;
  }
  // Advance one step in the sendbuffer
  // Actually shift it towards left (remove the very first letter)
  // Go 2 if skip == TRUE and 1 if skip == FALSE
  if((sendbufferlength - (1 + 1*skip)) >= 0)
  {
    memmove(sendbuffer,sendbuffer+(1 + 1*skip),sendbufferlength);
    sendbufferlength -= (1 + 1*skip);
  }
  else
  {
    sendbufferlength = 0;
    sendBuffer = FALSE;
    sendMem = FALSE;
#if USELCD == TRUE
    LCD.Status();
#endif
  }
}

// Add a string to the charbuffer
void sendString(char const *string)
{
  int i, length = strlen(string);

  sidetoneShift = 0;

  for(i = 0; i < length; i++)
  {
    if(string[i] == '0' && shortFigures)
      charBuffer[charBufferlength++] = 'T';
    else if(string[i] == '9' && shortFigures)
      charBuffer[charBufferlength++] = 'N';
    else
      charBuffer[charBufferlength++] = string[i];
  }

  charBuffer[charBufferlength]   = 0;
}

void sendConfigString(char const *string)
{
  charBuffer[charBufferlength++] = 0x01;

  sendString(string);

  charBuffer[charBufferlength++] = 0x02;
  charBuffer[charBufferlength]   = 0;
}

char WinkeyBuffer[2];

void addnextChar()
{
  if(sendWinkey && WinkeyBuffer[0] != 0)
  {
    // Return character as it is added to the keying memory to get
    // the next one
    Serial.write(WinkeyBuffer);
    WinkeyBuffer[0] = 0;
  }

  if(charBufferlength > 0)
  {
    WinkeyBuffer[0] = charBuffer[0];
    WinkeyBuffer[1] = 0;

    // If receiving characters while in memory mode pass
    // them to the recording routine
    if((configFlag == recMem || configFlag == recCall) && sendWinkey)
      configBuffer = charBuffer[0];

    sendCharacter(WinkeyBuffer);
    // Have to send character after sendCharacter
    // call since there could be $ which has first
    // to be replaced by the first letter of the callsign

    if(sendWinkey)
    {
      winkeyIDLEtimer = millis();

      if(!winkeyBUSY)
      { // Send BUSY
        Serial.write(0xC4);
        winkeyBUSY = TRUE;
      }
    }

    if (charBuffer[0] == 0)
    {
      charBufferlength = 0;
    }else{
      // Shift buffer one to the left (remove character just added to the sendbuffer)
      memmove(charBuffer,charBuffer+1,charBufferlength);
      charBufferlength--;
    }
  }
}

// Add single characters to the sendbuffer
void sendCharacter(char *letter)
{
  int i,j,length;

  if(letter[0] == '$')
    letter[0] = sendCallsign();

  if(letter[0] == 0x01) // Shift sidetone
    sendbuffer[sendbufferlength++] = 0x01;

  else if(letter[0] == 0x02) // Reset sidetone
    sendbuffer[sendbufferlength++] = 0x02;

  else if(letter[0] == 0x7C) // WinKey 1/2-pause sign
  {
    sendbuffer[sendbufferlength++] = 'H';
#if USELCD == TRUE
    // Print space if next char is again an 1/2-pause sign
    // otherwise ignore
    if(WinkeyPause > 2)
    {
      LCD.print(' ');
      WinkeyPause = 0;
    }
    else
      WinkeyPause++;
#endif
  }
  else if(letter[0] == ' ')
  { // additional 4 x ditlength (doing so is better than looping)
    sendbuffer[sendbufferlength++] = ' ';
    sendbuffer[sendbufferlength++] = ' ';
    sendbuffer[sendbufferlength++] = ' ';
    sendbuffer[sendbufferlength++] = ' ';

    if(((useKeyboard >= 2) && sendMem) || useKeyboard >= 3)
      KB.print(" ");
#if USELCD == TRUE
    LCD.print(' ');
    WinkeyPause = 0;
#endif

  }
  // Detect it prosign should be generated
  else if(letter[0] == 0x1B)
  {
    ProSign = TRUE;
  }
  else
  {
    for(i = 0; i < ALPHABETSIZE; i++)
    {
      if(strcasecmp(letter,morsecode[i][1]) == 0)
      {
        // Get lenght of string that will be send out
        length = strlen(morsecode[i][0]);

        for(j = 0; j < length; j++)
        {
          sendbuffer[sendbufferlength++] = morsecode[i][0][j];
        }
        // Add additional pause between characters only if not a prosign
        if(ProSign)
        {
          ProSign = FALSE;
        }
        else
        {
          sendbuffer[sendbufferlength++] = ' ';
          sendbuffer[sendbufferlength++] = ' ';
        }

        if(((useKeyboard >= 2) && sendMem) || useKeyboard >= 3) // 2 = keyboard memory, 3 = keyboard all buffers
          KB.print((char)correctKeycode(morsecode[i][1]));
#if USELCD == TRUE
        switch(letter[0])
        {
            case '<':
            LCD.print('A');
            LCD.print('R');
            break;

            case '>':
            LCD.print('S');
            LCD.print('K');
            break;

            default:
            LCD.print(letter[0]);

        }
        WinkeyPause = 0;
#endif
      }
    }
  }
  return;
}

// Clear sendbuffer and reset memory-sending state
void clearSendbuffer()
{
#if WINKEYEMULATION == TRUE
  sendWinkey = FALSE;
#endif
  sendBuffer = FALSE;

  sendMem = FALSE;
  charBufferlength = 0;
  sendbufferlength = 0;
  sidetoneShift = 0;
  WinkeyPause = FALSE;

#if USELCD == TRUE
  LCD.Status();
#endif

  if(winkeyBUSY)
  {
//    Serial.write(0xC2);
    while(Serial.read() != -1);
    Serial.write(0xC0);
    winkeyBUSY = FALSE;
  }
}

