#include "routines.h"

// The arduino Keyboard library is only working correctly when the
// user uses a keyboard with US-layout. Since many use other layout,
// we have to correct for that here.
//
// Example: I want to type 'Y'. The library sends the keycode 29 to
// the operating system. Unfortunately, this keycode corresponds to
// 'Z' on the German keyboard layout... While all the other letters
// and numbers are fine, virtually no symbol works without this
// correction we apply here.
char correctKeycode(char const *USKey)
{
#if LAYOUT == US
	// We do not have to correct for anything
	return USKey[0];

#elif LAYOUT == DE
    int keycode;
    // Check if special character
    if(strcasecmp(USKey,"ae") == 0)
      keycode = 1;
    else if(strcasecmp(USKey,"oe") == 0)
      keycode = 2;
    else if(strcasecmp(USKey,"ue") == 0)
      keycode = 3;
    else if(strcasecmp(USKey,"ss") == 0)
      keycode = 4;
    else
      keycode = USKey[0];

	switch (keycode)
	{
	case 'y':
		return 'z';

	case 'z':
		return 'y';

	case '=':
		return ')';

	case '/':
		return '&';

	case '?':
		return '_';

	case '!':
		return '!';

	case '\'':
		return '|';

	case '\"':
		return '@';

	case '(':
		return '*';

	case ')':
		return '(';

	case '+':
		return ']';

	case '-':
		return '/';

	case ':':
		return '>';

	case ';':
		return '<';

	case 1: // ae
		return '\'';

	case 2: // oe
		return ';';

	case 3: // ue
		return '[';

	case 4: // ss
		return '-';

	case '@':
	    KB.press(0x86); // KEY_RIGHT_ALT
		return 'q'; // Alt-Gr will be released by the evaluateCharacter routine afterwards

	case '_':
		return '?'; // underscore

	default:
		return USKey[0];
	}
#else
	#abort
#endif
}
