#include "routines.h"

#if USERELAYS == TRUE

#if ARDUINO_NANO == TRUE
const byte relaylist[10] = {1, 0, 2, 3, 4, 5, 6, 7, 8, 9};
#elif ATMEGA328P == TRUE
// Atmega 328P, Relais an PD7 :PD2 und PC4:PC1
const byte relaylist[10] = {2, 3, 4, 5, 6, 7, 15, 16, 17, 18};
#else
const byte relaylist[10] = {3, 2, 0, 1, 4, 8, 6, 7, 5, 9};
#endif

void RelayInit()
{
  for(byte i = 0; i < 10; i++)
  {
    pinMode(relaylist[i], OUTPUT);
    digitalWrite(relaylist[i], LOW);
  }
}

// switch relais
void RelaySet(byte dataH, byte dataL)
{
  // Check for bit status and set relais status accordingly
  for(byte i = 0; i < 2; i++)
  {
    RelaySingle(relaylist[8 + i], (dataH & (1 << i)));
  }
  for(byte i = 0; i < 8; i++)
  {
    RelaySingle(relaylist[i], (dataL & (1 << i)));
  }
}

void RelaySingle(byte nr, byte status)
{
  if(status)
    digitalWrite(nr, HIGH);
  else
    digitalWrite(nr, LOW);
}
#endif
