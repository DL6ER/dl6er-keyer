#include "routines.h"
#define MAGIC	0x7F

void writeSettingsToEEPROM()
{ // Ensure that we will not be interrupted by the WDT since writing to EEPROM takes some time
  // 3.3msec per byte according to the manual
  wdt_disable();
  byte write_buffer;
  /******* IMPORTANT *******/
  // Only write to EEPROM cells
  // which have changed to avoid
  // EEPROM wearing

  // EEPROM Memory Table
  // Byte 0: Magic byte
  eeprom_update_byte((uint8_t *)0, MAGIC);

  // Byte 1: Settings Byte 1
  write_buffer = 0;
  // Bit 0 = sidetoneActive
  write_buffer += (sidetoneActive) ? 1 : 0;
  // Bit 1 = key_output
  write_buffer += (key_output == 1) ? 2 : 0;
  // Bit 2 = toggleDitDah
  write_buffer += (toggleDitDah) ? 4 : 0;
  // Bit 3 = TRXMute
  write_buffer += (TRXMute) ? 8 : 0;
  // Bit 4 = TRXActive
  write_buffer += (TRXActive) ? 16 : 0;
  // Bit 5 = keyboardAutospace
  write_buffer += (keyboardAutospace) ? 32 : 0;
  // Bit 6 = useKeyboard 1 = PADDLE
  write_buffer += (useKeyboard & 1) ? 64 : 0;
  // Bit 7 = useKeyboard 2 = MEMORY, 3 = ALL
  write_buffer += (useKeyboard & 2) ? 128 : 0;
  eeprom_update_byte((uint8_t *)1, write_buffer);

  // Byte 2: Reserved

  // Byte 3-4: WpM
  eeprom_update_word((uint16_t *)3, WpM);

  // Byte 5-6: sidetoneFreq
  eeprom_update_word((uint16_t *)5, sidetoneFreq);

  // Byte 7: mode
  eeprom_update_byte((uint8_t *)7, mode);

  // Byte 9: charTolerance
  eeprom_update_byte((uint8_t *)8, charTolerance);

  // Byte 10: pttLeadInTime
  eeprom_update_byte((uint8_t *)9, pttLeadInTime);

  // Byte 11: pttTailTime
  eeprom_update_byte((uint8_t *)10, pttTailTime);

  // Byte 12: pttHangTime
  eeprom_update_byte((uint8_t *)11, pttHangTime);

  // Byte 13: beaconInterval
  eeprom_update_byte((uint8_t *)12, beaconInterval);

  // Byte 14: FarnsworthTiming
  eeprom_update_byte((uint8_t *)13, Farnsworth);

}

int readSettingsFromEEPROM()
{
  if(eeprom_read_byte((uint8_t *)0) != MAGIC)
  	return 0;

  byte read_buffer = eeprom_read_byte((uint8_t *)1);
  sidetoneActive = (read_buffer & 1) ? TRUE : FALSE;
  key_output = (read_buffer & 2) ? 1 : 2;
  toggleDitDah = (read_buffer & 4) ? TRUE : FALSE;
  TRXMute = (read_buffer & 8) ? TRUE : FALSE;
  TRXActive = (read_buffer & 16) ? TRUE : FALSE;
  keyboardAutospace = (read_buffer & 32) ? TRUE : FALSE;
  useKeyboard = (read_buffer & 64) ? 1 : 0;
  useKeyboard += (read_buffer & 128) ? 2 : 0;

/*  read_buffer = eeprom_read_byte((uint8_t *)2);  */

  WpM = eeprom_read_word((uint16_t *)3);
  sidetoneFreq = eeprom_read_word((uint16_t *)5);
  mode = eeprom_read_byte((uint8_t *)7);
  charTolerance = eeprom_read_byte((uint8_t *)8);
  pttLeadInTime = eeprom_read_byte((uint8_t *)9);
  pttTailTime = eeprom_read_byte((uint8_t *)10);
  pttHangTime = eeprom_read_byte((uint8_t *)11);
  beaconInterval = eeprom_read_byte((uint8_t *)12);
  Farnsworth = eeprom_read_byte((uint8_t *)13);

  return 1;
}

void initCallsign()
{
  eeprom_update_byte((uint8_t *)50, 'D');
  eeprom_update_byte((uint8_t *)51, 'L');
  eeprom_update_byte((uint8_t *)52, '6');
  eeprom_update_byte((uint8_t *)53, 'E');
  eeprom_update_byte((uint8_t *)54, 'R');
  eeprom_update_byte((uint8_t *)55, 0);

  return;
}

void writeCallsign()
{
  int address = 0;

  while(memoryBuffer[address] != 0 && address <= MAXCALLSIGNLENGTH)
  {
    eeprom_update_byte((uint8_t *)(50 + address), memoryBuffer[address]);
    address++;
  }
  // Finally zero terminte the string
  eeprom_update_byte((uint8_t *)(50 + address), 0);

  return;
}

char sendCallsign()
{
  char memorybuffer[MAXCALLSIGNLENGTH];
  int counter = 0, i = 0;
  eeprom_read_block(&memorybuffer, (uint8_t *)50, MAXCALLSIGNLENGTH);

  // Get length of callsign
  while(memorybuffer[counter] != 0)
    counter++;

  for(i=1;i<counter;i++)
  {
    charBuffer[0] = memorybuffer[counter-i];
    memmove(charBuffer+1,charBuffer,charBufferlength);
    charBufferlength++;
  }
  return memorybuffer[0];
}
