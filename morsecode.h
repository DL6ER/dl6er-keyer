char const *morsecode[][2] = {
  {"ET",	"a"},
  {"TEEE",	"b"},
  {"TETE",	"c"},
  {"TEE",	"d"},
  {"E",		"e"},
  {"EETE",	"f"},
  {"TTE",	"g"},
  {"EEEE",	"h"},
  {"EE",	"i"},
  {"ETTT",	"j"},
  {"TET",	"k"},
  {"ETEE",	"l"},
  {"TT",	"m"},
  {"TE",	"n"},
  {"TTT",	"o"},
  {"ETTE",	"p"},
  {"TTET",	"q"},
  {"ETE",	"r"},
  {"EEE",	"s"},
  {"T",		"t"},
  {"EET",	"u"},
  {"EEET",	"v"},
  {"ETT",	"w"},
  {"TEET",	"x"},
  {"TETT",	"y"},
  {"TTEE",	"z"},
  {"ETTTT",	"1"},
  {"EETTT",	"2"},
  {"EEETT",	"3"},
  {"EEEET",	"4"},
  {"EEEEE",	"5"},
  {"TEEEE",	"6"},
  {"TTEEE",	"7"},
  {"TTTEE",	"8"},
  {"TTTTE",	"9"},
  {"TTTTT",	"0"},
  {"ETETET","."},
  {"TEEET",	"="},
  {"TTEETT",","},
  {"TEETE",	"/"},
  {"EETTEE","?"},
  {"TETETT","!"},
  {"ETEETE","\""},
  {"ETTTTE","'"},
  {"TETTE", "("}, // also prosign KN
  {"TETTET",")"}, // also prosign KK
  {"ETETE", "+"}, // also prosign AR
  {"TEEEET","-"},
  {"TTTEEE",":"},
  {"TETETE",";"},
#if LAYOUT == DE
  {"ETET","ae"},
  {"EETT","ue"},
  {"TTTE","oe"},
  {"EEETTEE","ss"},
#endif
  {"ETTETE","@"},
  // Callsign shortcut
  {"TETEET","$"},
  // Prosigns have to written
  // in capital letters
  {"TETETTET","CQ"}, // only for display/keyboard
  {"EEETET","SK"}, // only for display/keyboard
//  {"ETETE","AR"}, // NOTE: AR is identical to plus sign ( + )
  {"TEEETET","BK"}, // only for display/keyboard
  {"ETEEE","AS"}, // only for display/keyboard
  {"TETTE","KN"}, // only for display/keyboard
  {"ETETE","<"}, // also prosign AR
  {"EEETET",">"} // also prosign SK
};

unsigned char ALPHABETSIZE = sizeof(morsecode)/(sizeof(morsecode[0]));
