#include "routines.h"

#if WINKEYEMULATION == TRUE
// Basic implementation of the Winkey protocol
void evaluateWinkey()
{
  // read the incoming byte:
  unsigned char serial_buffer = Serial.read();
  // If not Admin mode and host mode inactive -> ignore and return received char
  if((serial_buffer == 0x00 || serial_buffer == 0x7F || useWinkeyEmulation))
  // React on whatever is coming in
  switch(serial_buffer)
  {

  case 0x00: // Admin mode
    wait_for_serial_rx();
    serial_buffer = Serial.read();
    switch(serial_buffer)
    {
    case 0x04: // Echo mode (echo next character to the host)
      wait_for_serial_rx();
      serial_buffer = Serial.read();
      Serial.write(serial_buffer);
    break;

    case 0x02: // Activate host mode
      // Return something...
      useWinkeyEmulation = TRUE;
      sendConfigString("K");
#if USELCD == TRUE
      LCD.Status();
#endif
      Serial.write(0x23); // Send revision > 0x20
      Serial.write((unsigned char)(0x80 + (WpM & 0x3F))); // Speedpot value
      Serial.write(0xC0); // IDLE
      break;

    case 0x03: // Disable host mode (i.e. software closed)
      useWinkeyEmulation = FALSE;
#if USELCD == TRUE
      LCD.Status();
#endif
      break;
    }
  break; // Admin mode

  case 0x01: // Sidetone Control
    // Have to catch one byte
    wait_for_serial_rx();
    serial_buffer = Serial.read();
  break;

  case 0x02: // Set WpM
    wait_for_serial_rx();
    serial_buffer = Serial.read();
    setWpM((int)serial_buffer);
  break;

  case 0x03: // Weighting
    // Not supported!
    // Have to catch one byte
    wait_for_serial_rx();
    Serial.read();
  break;

  case 0x04: // PTT Lead-In/Tail
    wait_for_serial_rx();
    pttLeadInTime = Serial.read();
    wait_for_serial_rx();
    pttTailTime = Serial.read();
  break;

  case 0x05: // Speed Pot Setup
    // Have to catch three bytes
    wait_for_serial_rx();
    Serial.read();
    wait_for_serial_rx();
    Serial.read();
    wait_for_serial_rx();
    Serial.read();
    // Not supported!
  break;

  case 0x06: // Pause morse output
    // Not supported!
    // Have to catch one byte
    wait_for_serial_rx();
    Serial.read();
  break;

  case 0x07: // Speedpot query
    // Return value in WpM in 6bit format
    Serial.write((unsigned char)(0x80 + (WpM & 0x3F)));
  break;

  case 0x08: // Erase the last character in the sendbuffer
    // Not supported!
  break;

  case 0x09: // Set PinConfig <- set KEY-output to 1 or 2
    wait_for_serial_rx();
    serial_buffer = Serial.read();
    if(serial_buffer & 0x04)
    {
      key_output = 1;
    }
    else if(serial_buffer & 0x08)
    {
      key_output = 2;
    }
#if USELCD == TRUE
    LCD.Status();
#endif
  break;

  case 0x0A: // Interrupt sending
    rx();
    clearSendbuffer();
//    Serial.write(0xC0);
  break;

  case 0x0B: // Tune mode
    wait_for_serial_rx();
    serial_buffer = Serial.read();
    if(serial_buffer)
    { // Key TRX
      key_trx = TRUE;
      tx();
#if USELCD == TRUE
	    LCD.Row("Tune");
#endif
    }
    else
    { // Unkey TRX
      key_trx = FALSE;
      rx();
#if USELCD == TRUE
	    LCD.Row("");
#endif
      // Send IDLE
      Serial.write(0xC0);
    }
  break;

  case 0x0C: // HSCW Speed
    // Not supported!
    // Have to catch one byte
    wait_for_serial_rx();
    Serial.read();
  break;

  case 0x0D: // Set Farnsworth speed
    // Not supported!
    // Have to catch one byte
    wait_for_serial_rx();
    Serial.read();
  break;

  case 0x0E: // Setwinkey2 mode
    wait_for_serial_rx();
    serial_buffer = Serial.read();
    evaluateWinkeyModebyte(serial_buffer);
  break;

  case 0x0F: // Receive default values
    // Byte 1: Mode Register -> paddle swap? mode?
    wait_for_serial_rx();
    serial_buffer = Serial.read();

    evaluateWinkeyModebyte(serial_buffer);

    // Byte 2: Speed in WpM
    wait_for_serial_rx();
    serial_buffer = Serial.read();
    setWpM((int)(serial_buffer & 0x3F));

    // Byte 3 - 15: unused
    for(int i = 3; i <= 15; i++)
    {
      wait_for_serial_rx();
      serial_buffer = Serial.read();
    }
  break;

  case 0x10: // Setup first extension
    // Not supported!
    // Have to catch one byte
    wait_for_serial_rx();
    Serial.read();
  break;

  case 0x11: // Set keying compensation
    // Not supported!
    // Have to catch one byte
    wait_for_serial_rx();
    Serial.read();
  break;

  case 0x12: // Paddle switchpoint
    // Not needed!
    // Have to catch one byte
    wait_for_serial_rx();
    Serial.read();
  break;

  case 0x13: // Null Command
    // This does nothing ...
  break;

  case 0x14: // Software Paddle Control
    // Not supported because I think this is nonsense.
    // Have to catch one byte
    wait_for_serial_rx();
    Serial.read();
  break;

  case 0x15: // Status query
    if(winkeyBUSY)
      Serial.write(0xC4);
    else
      Serial.write(0xC0);
  break;

  case 0x16: // Input Buffer Command
    // Not supported!
    // Have to catch one byte
    wait_for_serial_rx();
    Serial.read();
  break;

  case 0x17: // Dit/dah ratio
    // Not supported!
    // Have to catch one byte
    wait_for_serial_rx();
    Serial.read();
  break;

  case 0x18: // PTT control
    wait_for_serial_rx();
    serial_buffer = Serial.read();

    if(serial_buffer)
    {
      keyActive = TRUE;
      ptt_on();
    }
    else
    {
      keyActive = FALSE;
      ptt_off();
    }

  break;

  case 0x7F: // UcxLog command sequence
    wait_for_serial_rx();
    serial_buffer = Serial.read();
    // Respond with 0x7F if 0x7F has been send twice!
    if(serial_buffer == 0x7F)
    {
      Serial.write(0x7F);
    }
    else if(serial_buffer < 4 && useWinkeyEmulation)
    { // check if only the lowest two bits are set to allow future implementations of additional commands
      wait_for_serial_rx();
      unsigned char serial_buffer2 = Serial.read();
#if USERELAYS == TRUE
      RelaySet(serial_buffer, serial_buffer2);
#endif
    }
    break;

  // If it is no control command than it should be a character
  // Also save if it is the comtrol sign (0x1B - prosign request)
  default:
    if(useWinkeyEmulation && (serial_buffer == 0x1B || serial_buffer >= 0x20))
    {
      sendWinkey = TRUE;
      charBuffer[charBufferlength++] = serial_buffer;
      charBuffer[charBufferlength]   = 0;
    }
    break;
  }
}

void evaluateWinkeyModebyte(char serial_buffer)
{
  if(serial_buffer & 8) // Bit 3 = Paddle Swap
    toggleDitDah = TRUE;
  else
    toggleDitDah = FALSE;

  if(serial_buffer & 16)
  {
    if((serial_buffer & 32)) // Bit 5,4 = (11) Bug mode
      mode = BUG;
    else					 // Bit 5,4 = (01) Iambic A mode
      mode = IAMBIC_A;
  }
  else
  {
    if((serial_buffer & 32)) // Bit 5,4 = (10) Ultimatic mode
      mode = ULTIMATIC;
    else					 // Bit 5,4 = (00) Iambic B mode
    mode = IAMBIC_B;
  }
}

// Avoid hanging up if there is no serial communication due to some unknown reason
void wait_for_serial_rx()
{
  for(int i = 0; i < 10000; i++)
    if(Serial.available())
      return;
}


#endif
