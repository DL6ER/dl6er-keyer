#include "routines.h"

// Evaluate sent character and send it to the PC
void evaluateCharacter()
{
  int i;
  char letter;

  for(i = 0; i < ALPHABETSIZE; i++)
  {
    if(strcasecmp(ditdahbuffer,morsecode[i][0]) == 0) {
      // Output if not from Memory or from WinkeyEmulation
      if(!sendBuffer)
      { // Are we within config mode?
        if(config)
          configBuffer = morsecode[i][1][0];
        if(!config || configFlag == recMem || useKeyboard >= 3)
        { // Get the letter and process it further
          if(useKeyboard)
          { // Check if led in by capital letter == Prosign
            if(morsecode[i][1][0] >= 0x41 && morsecode[i][1][0] <= 0x5A)
            {
              KB.print(morsecode[i][1]);
              return;
            }
            letter = (char)correctKeycode(morsecode[i][1]);
            KB.print(letter);
#if LAYOUT == DE
            if(strcasecmp(morsecode[i][1],"@") == 0)
              KB.release(0x86); // KEY_RIGHT_ALT
#endif
            if(shift)
            {
              KB.release(0x81); // KEY_LEFT_SHIFT
              shift = FALSE;
            }
          }
#if WINKEYEMULATION == TRUE
          if(useWinkeyEmulation)
            Serial.write(morsecode[i][1]);
#endif
#if USELCD == TRUE
			int j, length;
			if(sidetoneShift == 0){
				length = strlen(morsecode[i][1]);
				for(j = 0; j < length; j++)
				{
					letter = morsecode[i][1][j];
					LCD.print(letter);
				}
			}
#endif
        }
      }
      return;
    }
  }
  // Prevent auto-space if it is not a character from the list
  keyedsomething = FALSE;

  // not found + only dits (not T)? -> Backspace
  if(!strstr(ditdahbuffer,"T"))
  {
    if(!sendMem && !sendWinkey && useKeyboard)
    {
      KB.press(0xB2); // KEY_BACKSPACE
      delayMicroseconds(200);
      KB.release(0xB2); // KEY_BACKSPACE

#if WINKEYEMULATION == TRUE
      if(useWinkeyEmulation)
        Serial.write("\b");
#endif
#if USELCD == TRUE
      LCD.print(1);
#endif
    }
    if(config && ((configFlag == recMem && memoryCounter > 0) || configFlag == recCall))
    { // Prevent memory leak
      if(memoryCounter > 0)
        memoryCounter--;
      // Announce last 3 characters
      char lastchar[4];
      byte i = 0;
      if(memoryCounter > 2)
        lastchar[i++] = memoryBuffer[memoryCounter - 3];
      if(memoryCounter > 1)
        lastchar[i++] = memoryBuffer[memoryCounter - 2];
      if(memoryCounter > 0)
        lastchar[i++] = memoryBuffer[memoryCounter - 1];
      lastchar[i++] = 0;
      sendConfigString(lastchar);
    }
    return;
  }

  // not found + only dahs (no E)? -> Space
  if(!strstr(ditdahbuffer,"E"))
  {
    if(!sendMem && !sendWinkey && useKeyboard)
    {

      KB.print(" ");

#if WINKEYEMULATION == TRUE
      if(useWinkeyEmulation)
        Serial.write(" ");
#endif
#if USELCD == TRUE
       LCD.print(' ');
#endif
    }
    if(config && configFlag == recMem)
      configBuffer = ' ';
    return;
  }

  // TETETE... ? -> Return
  if(strstr(ditdahbuffer,"TETETE") == &ditdahbuffer[0])
  {
    if(!sendMem && !sendWinkey && useKeyboard && configFlag != recMem)
    {

      KB.print("\n");

#if WINKEYEMULATION == TRUE
      if(useWinkeyEmulation)
        Serial.write("\n");
#endif
    }
    return;
  }

  // ETETETET... ? -> Shift
  if(strstr(ditdahbuffer,"ETETET") == &ditdahbuffer[0])
  {
    if(!sendMem && !sendWinkey && useKeyboard && configFlag != recMem)
    {
      if(!shift){
        KB.press(0x81); // KEY_LEFT_SHIFT
        shift = TRUE;
      }
      else
      {
        KB.release(0x81); // KEY_LEFT_SHIFT
        shift = FALSE;
      }
    }
    return;
  }

  // still not found? -> unknown

#if DEBUG == TRUE
  KB.print("UNKNOWN ");
  KB.println(ditdahbuffer);
#else
  if(useKeyboard)
    KB.print(correctKeycode("_"));
#endif // DEBUG == TRUE

}

// Clear dit-dah information after evaluating a character
void clearDitDahBuffer()
{
  for(int i=0; i < MAXDITDAHLEN; i++)
    ditdahbuffer[i] = 0;
}
