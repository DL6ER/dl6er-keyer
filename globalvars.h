extern char const *morsecode[][2];
extern byte ALPHABETSIZE;

// status
extern int sidetoneShift;
extern char dit;
extern char dah;
extern char menuswitch;
extern char state;
extern unsigned long time;
extern unsigned long memTimer;
extern unsigned long keyOffTime;
extern unsigned long ditlength;
extern unsigned long ditlength_pause;
extern char ditdahbuffer[MAXDITDAHLEN];
extern byte ditdahcounter;
extern char useWinkeyEmulation;
extern char sendWinkey;
extern char lastWinkeyChar;
extern char winkeyBUSY;
extern unsigned int winkeyIDLEtimer;
extern char shift;
extern char key_trx;
extern char config;
extern char programmingMode;
extern char sendBuffer;
extern char configBuffer;
extern char configFlag;
extern char shortFigures;
extern char beaconMode;
extern byte keyedsomething;
extern byte memoryCounter;
extern byte memoryNr;
extern byte Farnsworth;

extern char memoryBuffer[MAXMEMORYLENGTH];
extern char sendbuffer[MAXSENDBUFFERLEN];

extern char charBuffer[CHARBUFFERLENGTH];
extern byte charBufferlength;

extern unsigned int sendbufferlength;
extern char sendMem;

extern byte pttTailTime;
extern byte pttHangTime;
extern byte pttLeadInTime;
extern byte pttActive;
extern byte keyActive;

// Bits
extern byte sidetoneActive;
extern byte key_output;
extern byte toggleDitDah;
extern byte useKeyboard;
extern byte TRXMute;
extern byte TRXActive;

extern int WpM;
extern unsigned int sidetoneFreq;
extern byte mode;
extern byte charTolerance;
extern byte beaconInterval;
extern byte keyboardAutospace;

extern unsigned long toleranceTime;
extern byte PaddleState;
extern byte PaddleMemory[PADDLEBUFFERLENGTH];
extern byte PaddleMemoryLength;
extern unsigned long PaddleTimer;

extern byte waiting_for_PTT;

extern unsigned long ditTimer, dahTimer, debounceTimer;

// Keyboard class
extern KB_ KB;

// LCD class
#if USELCD == TRUE
extern LCD_ LCD;
#endif
