/***** Some necessary definitions *****/
#define TRUE  1
#define FALSE 0
/***** User settings *****/
// Keyboard-Layout (needed for correct character output)
#define LAYOUT DE
// Use Winkey emulation (connectivity to logbook software for direct keying)?
// Default: TRUE
#define WINKEYEMULATION TRUE
// Evaluate memory keying buttons only during menu?
// Default: TRUE
#define SENDMEMORYONLYDURINGMENU TRUE

// Do we use the bandport output?
#define USERELAYS TRUE

// Do we want the startup message to be played?
// Usually we want that. However, there are some Arduinos
// which automatically reset themselves each time a software
// tries to connect. With this setting set to FALSE, resets
// such resets should stay unnoticed by the user.
#define STARTUPMSG TRUE

// Configure for
// - Pololu A* 32U4 Micro
#define POLOLU_MICRO FALSE
// - Arduino Nano
#define ARDUINO_NANO FALSE
// - ATMEGA 328P
#define ATMEGA328P FALSE
// if everything above is FALSE, then configure for Arduino Pro Micro
// (as described in the FUNKAMATEUR journal)

#if USERELAYS == TRUE
#define USELCD FALSE
#else
#define USELCD TRUE
#endif

/***** Using a potentiometer to control the keying speed ******/
/* To enable this feature, uncomment the following lines      */
//#define CONN_POTI     9
//#define POTI_WPM_MIN  15
//#define POTI_WPM_MAX  35
/**************************************************************/

/***** Using a rotary encoder to control the keying speed ******/
/* To enable this feature, uncomment the following lines       */
/* Added thanks to a contribution from Stojan, DK4SR           */
//#define USE_ROTARY_ENCODER
//#define OPTION_ENCODER_HALF_STEP_MODE // Half-step mode?
//#define rotary_pin1 14                // CW Encoder Pin
//#define rotary_pin2 15                // CCW Encoder Pin
/**************************************************************/


/***** Physical connection to the world *****/
#if POLOLU_MICRO == TRUE
// Default settings for Pololu A* 32U4 (DB6EDR)
// DAH input to the 3.5mm jack for the paddle
#define CONN_DAH          10 // D10
// DIT input to the 3.5mm jack for the paddle
#define CONN_DIT          11 // D16
// Output to the TRX1
#define CONN_KEY1         12 // A3
#define CONN_PTT1         15 // ISP - upper left
// KEY2 output to the TRX2
#define CONN_KEY2         13 // internal LED
#define CONN_PTT2         14 // ISP - lower left
// SPEAKER output for the sidetone speaker
#define CONN_SPEAKER      18 // A0
// PUSHSWITCH input (needed to be able to enter Menu)
#define CONN_SWITCH       19 // A1

#elif ARDUINO_NANO == TRUE
// Default settings for Arduino Nano (DL8DWW)
// DAH input to the 3.5mm jack for the paddle
#define CONN_DAH          11 // D11
// DIT input to the 3.5mm jack for the paddle
#define CONN_DIT          12 // D12
// Output to the TRX1
#define CONN_KEY1         18 // A4
#define CONN_PTT1         19 // A5
// KEY2 output to the TRX2
#define CONN_KEY2         16 // A2
#define CONN_PTT2         17 // A3
// SPEAKER output for the sidetone speaker
#define CONN_SPEAKER      14 // A0
// PUSHSWITCH input (needed to be able to enter Menu)
#define CONN_SWITCH       15 // A1


#elif ATMEGA328P == TRUE
// Default settings for ATMEGA 328P (DL8DWW)
// DAH input to the 3.5mm jack for the paddle
#define CONN_DAH          13 // PB5
// DIT input to the 3.5mm jack for the paddle
#define CONN_DIT          12 // PB4
// Output to the TRX1
#define CONN_KEY1         10 // PB2
#define CONN_PTT1         11 // PB3
// KEY2 output to the TRX2
#define CONN_KEY2         8 // PB0
#define CONN_PTT2         9 // PB1
// SPEAKER output for the sidetone speaker
#define CONN_SPEAKER      14 // PC0
// PUSHSWITCH input (needed to be able to enter Menu)
#define CONN_SWITCH       19 // PC5

#else
// Default settings for Arduino Pro Micro (DL6ER)
// DAH input to the 3.5mm jack for the paddle
#define CONN_DAH          10 // D10
// DIT input to the 3.5mm jack for the paddle
#define CONN_DIT          16 // D16
// Output to the TRX1
#define CONN_KEY1         21 // A3
#define CONN_PTT1         15 // D15
// KEY2 output to the TRX2
#define CONN_KEY2         20 // A2
#define CONN_PTT2         14 // D14
// SPEAKER output for the sidetone speaker
#define CONN_SPEAKER      18 // A0
// PUSHSWITCH input (needed to be able to enter Menu)
#define CONN_SWITCH       19 // A1

#endif

// Internal states (mainly for CW timing purposes)
#define NONE  0
#define DIT   1
#define DAH   2
#define DITPAUSE 3
#define DAHPAUSE 4
#define EVALUATECHAR 5
#define LETTERPAUSE 6
#define INTRALETTERPAUSE 7

// Keying modes
#define IAMBIC_A	1
#define IAMBIC_B	2
#define ULTIMATIC	3
#define BUG			4
#define STRAIGHT	5
#define SINGLELEVER	6

// Keyboard Layout
#define US 1
#define DE 2

// Config submodes
#define setKeyingMode   	1
#define setSidetoneFreq	 	2
#define PROGRAMM       	 	3
#define setWPM				4
#define setTolerance		5
#define setBeaconInterval	6
#define recMem				7
#define setLeadInTime		8
#define setHangTime			9
#define setTailTime			10
#define setFarnsworth		11
#define setKeyboardMode		12
#define recCall				13

// Minimum WpM
// 2 WpM = 10 CpM
#define MINWPM 2
// Maximum WpM
// 60 WpM = 300 CpM
#define MAXWPM 60

// Display lines
#define LCD_WIDTH 16
#define LCD_HEIGHT 2
#define DISPLAY_TIMEOUT 1500
#define DISPLAY_RS 2
#define DISPLAY_EN 3
#define DISPLAY_D4 4
#define DISPLAY_D5 5
#define DISPLAY_D6 6
#define DISPLAY_D7 7
//#define DISPLAY_BL 9

#define DEBUG FALSE

#define MAXDITDAHLEN 32
#define MAXSENDBUFFERLEN 32
#define PADDLEBUFFERLENGTH 16
// Do not change this!
// If you do, you will have to reprogram
// all memories + expect strange behaviour
#define MAXMEMORYLENGTH 100
#define MAXCALLSIGNLENGTH 20

#define CHARBUFFERLENGTH 120
