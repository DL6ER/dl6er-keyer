#include "routines.h"

// Evaluate Straight Keying
/*************************************************
Key the transceiver when one of the input lines
is keyed.
*************************************************/
void Straight()
{
  if(dit || dah)
  {
    if(config)
      key_trx = FALSE;
    else
      key_trx = TRUE;

    if(!pttActive && !TRXMute && key_trx)
    {
      ptt_on();
      if(pttLeadInTime)
      { // LeadInTime is given in units of 10msec
        delay(10*pttLeadInTime);
        // Do this in order to avoid automatic droping of PTT if LeadIn time was high
        keyOffTime = micros();
        // Return so we can check if dit or dah is still pressed in the next round
        return;
      }
    }
    tx();
  }
  else
  {
    if(config)
      key_trx = FALSE;
    else
      key_trx = TRUE;

    if(keyActive)
      rx();
  }
}
