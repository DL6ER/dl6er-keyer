// Operation parameters
byte sidetoneActive = TRUE;
byte key_output = 1;
// 1 = print paddle
// 2 = print memory
// 4 = print config
byte useKeyboard = 1;
byte TRXMute = FALSE;
byte TRXActive = HIGH;
byte keyboardAutospace = TRUE;


int WpM = 15;
unsigned int sidetoneFreq = 700;
byte mode = IAMBIC_A;
byte pttLeadInTime = 0;
byte pttTailTime = 0;
byte pttHangTime = 3;
byte beaconInterval = 4;
byte Farnsworth = 0;
byte charTolerance = 4;

byte PaddleState = 0;
byte PaddleMemory[PADDLEBUFFERLENGTH];
byte PaddleMemoryLength = 0;
unsigned long PaddleTimer = 0;

int sidetoneShift = 0;
char dit = FALSE;
char dah = FALSE;
char menuswitch = FALSE;
char state = NONE;
unsigned long time = 0;
unsigned long memTimer = 0;
unsigned long keyOffTime = 0;
unsigned long ditlength = 0;
unsigned long ditlength_pause = 0;
unsigned long toleranceTime = 0;
char ditdahbuffer[MAXDITDAHLEN];
byte ditdahcounter = 0;

char sendbuffer[MAXSENDBUFFERLEN];
unsigned int sendbufferlength = 0;
char memoryBuffer[MAXMEMORYLENGTH];
byte memoryCounter = 0;
byte memoryNr = 0;

char charBuffer[CHARBUFFERLENGTH];
byte charBufferlength = 0;

byte pttActive = FALSE;
byte keyActive = FALSE;

char key_trx = TRUE;
char config = FALSE;
char sendMem = FALSE;

char useWinkeyEmulation = FALSE;
char sendWinkey = FALSE;
char lastWinkeyChar = 0;
char winkeyBUSY = FALSE;
unsigned int winkeyIDLEtimer = 0;
char shift = FALSE;
char programmingMode = FALSE;
char sendBuffer = FALSE;
char configBuffer = 0;
char shortFigures = FALSE;
char configFlag = FALSE;
char toggleDitDah = FALSE;
char beaconMode = FALSE;
byte keyedsomething = FALSE;

byte waiting_for_PTT = FALSE;

unsigned long ditTimer = micros(), dahTimer = micros(), debounceTimer = micros();

// Keyboard class
KB_ KB;

#if USELCD == TRUE
LCD_ LCD;
char lcdBuffer[LCD_WIDTH+1];
byte lcdcounter = 0;

#if DISPLAY_BL != 0
long display_timer = 0;
byte display_byte = 0;
#endif
#endif

#if defined(CONN_POTI)
byte potiByte = 0;
#endif
