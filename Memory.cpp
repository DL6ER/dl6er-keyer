#include "routines.h"

// store these initializer in FLASH memory
const char init_memory0[] PROGMEM = "cq cq de $ cq cq de $ $ pse k";
const char init_memory1[] PROGMEM = "$";
const char init_memory2[] PROGMEM = "5nn tu";
const char init_memory3[] PROGMEM = "qrz? $";
const char init_memory4[] PROGMEM = "de $ - cfm - tnx fer fb qso 73 - de $ k";


  // Memory will be stored in EEPROM
  // Memory 1 (nr = 0): 100 - 199
  // Memory 2 (nr = 1): 200 - 299
  // Memory 3 (nr = 2): 300 - 399
  // ...
  // Memory 9 (nr = 8): 900 - 999


int sendMemory(byte nr)
{
  if(nr > 8)
    return -1;

  int start = 100 + ((int)nr)*MAXMEMORYLENGTH;
  char memorybuffer[MAXMEMORYLENGTH];

  eeprom_read_block(&memorybuffer, (uint8_t *)start, MAXMEMORYLENGTH);

  config = FALSE;
  clearSendbuffer();
  sendMem = nr+1;
  sendString(memorybuffer);
  memTimer = millis();

  return 0;
}

void initMemory(byte nr)
{
  const char *str;
  // At the moment only memory 0 and 1 are preset and can be initialized
  switch(nr)
  {
  case 0:
    str = init_memory0;
    break;
  case 1:
    str = init_memory1;
    break;
  case 2:
    str = init_memory2;
    break;
  case 3:
    str = init_memory3;
    break;
  case 4:
    str = init_memory4;
    break;
  default:
    return;
  }
  for(byte i = 0; i < MAXMEMORYLENGTH; i++)
  {
  	memoryBuffer[i] = pgm_read_byte(str++);
  	if(!memoryBuffer[i])
  	  break;
  }
  // Write read memory string to EEPROM
  writeMemory(nr);
}

int writeMemory(byte nr)
{
  if(nr > 8)
    return -1;

  int start = 100 + ((int)nr)*MAXMEMORYLENGTH;
  int stop = start + MAXMEMORYLENGTH -1;
  int address = start;

  while(memoryBuffer[(address - start)] != 0 && address <= stop)
  {
    eeprom_update_byte((uint8_t *)address, memoryBuffer[(address - start)]);
    address++;
  }
  // Finally zero terminte the string
  eeprom_update_byte((uint8_t *)address, 0);

  // return length of writen memory string
  return (address - start);
}


/*
void clearMemory(byte nr)
{
  eeprom_update_byte((uint8_t *)(100 + ((int)nr)*MAXMEMORYLENGTH), 0);
}
*/
