#include "routines.h"

// Evaluate Iambic (A/B) + Ultimatic Keying
/*************************************************
--- IAMBIC A ---
German description of Iambic A by qprproject.de
  In Mode A wird immer das Element
  vervollständigt, was im Moment
  des Öffnens beider Kontakte gerade
  gesendet wird. Lasse ich also
  mitten in einem DIT beide paddle
  gleichzeitig los, wird der Punkt zu
  Ende gesendet. Lasse ich mitten
  im DAH beide paddle los, wird das
  DAH zu Ende gesendet.

--- IAMBIC B ---
German description of Iambic B by qprproject.de
  In Mode B sendet die Elektronik aber
  beim gleichzeitigen Öffnen der Kontakte
  anschliessend noch das oppositionelle
  Element aus. Öffne ich also während
  der Sendung eines DITs beide Kontakte
  gleichzeitig, so wird nach vervollständigung
  des DIT noch zusätzlich ein DAH gesendet und
  umgekehrt.
This description has been the basis for the
requested Mode B implementation in here, since
I do not use and/or know it myself.

--- ULTIMATIC ---
Sverre Holm, LA3ZA wrote:
 When both paddles are squeezed, the last one to
 be pressed takes control. So when right-left is
 pressed one gets a dah followed by dits, not the
 dah-di-dah-dit of the iambic keyer.
From my point of view it is almost idential to the
IambicA mode with the only difference that dit
does not automatically has a priorized role after
sending a dah (and vice-versa). If both paddles
are pressed at the beginning of a new dit/dah then
the last one that has been played is repeated.

--- SINGLELEVER ---
Has been first proposed by Sverre Holm, LA3ZA

This is almost the same as Ultimatic, just "the
other way around". In contrast to the ULTIMATIC
mode, the _first_ pressed paddle has priority.
This emulates a single lever paddle.

See for instance "Single Paddle operation with
Iambic paddles" by Larry Winslow, W0NFU, in QST,
October 2009.
*************************************************/

void addDah(byte pos), addDit(byte pos);
void Iambic();

unsigned long ditpause = 0, dahpause = 0;
char last_added = 0;
byte iambic = FALSE, memUltimatic = FALSE;
byte dit_ = FALSE, dah_ = FALSE;

// Record paddle into memory buffer
void RecordPaddleMemory()
{
/*************************************** Debounce inputs ****************************************************/
  if(dit != dit_){ dit_ = dit; debounceTimer = micros(); }
  if(dah != dah_){ dah_ = dah; debounceTimer = micros(); }

  // Debouncing algorithm -> Use dit/dah value only if constant for at least ditlength/5 or 5 millisec (whatever is larger)
  // Press dit or dah
  if(((micros() - debounceTimer) < MAX(5000, ditlength/5)) && (dit_ || dah_))
    return;
  // Released dit or dah
  if(((micros() - debounceTimer) < MAX(2000, ditlength/15)))
    return;
/************************************************************************************************************/

/*************************************** Evaluate buffer-paddle state ***************************************/
  if(ditTimer < millis()) // ditTimer will be > millis() if keying inside ditpause
  { // If not in the middle of a character (i.e. last_sign == 0) then block a little longer
  // so that e can be keyed more easily
    // Block extra 40% of a ditlength for crisp characters
    if((millis() - ditTimer) < ((ditlength*14)/10000) && dit)
    {
      PaddleState &= ~1;
      PaddleState |= 2; // 2 = currently dit, can't do anything
    }
    else if((millis() - ditTimer) < ((ditlength*2)/1000))
    {
      PaddleState &= ~2;
      PaddleState |= 1; // 1 = currently ditpause, can accept next dit
    }
    else
      PaddleState &= ~3; // cleared, free
  }
  else
    // ditTimer will be > micros() if keying inside ditpause
    return;

  if(dahTimer < millis())
  {
    // Block extra 40% of a ditlength for crisp characters
    if((millis() - dahTimer) < ((ditlength*34)/10000) && dah)
    {
      PaddleState &= ~4;
      PaddleState |= 8; // 8 = currently dah, can't do anything
    }
    else if((millis() - dahTimer) < ((ditlength*4)/1000))
    {
      PaddleState &= ~8;
      PaddleState |= 4; // 4 = currently dahpause, can accept next dah
    }
    else
      PaddleState &= ~12; // cleared, free
  }
  else
    // dahTimer will be > micros() if keying inside ditpause
    return;
/************************************************************************************************************/

/*************************************** Clear up after keying **********************************************/
  if(!PaddleState)
  {
    if(iambic && mode == IAMBIC_A)
    {
      iambic = FALSE;
    }
    if((millis() - ditTimer) > ((ditlength*5)/2) && (millis() - dahTimer) > ((ditlength*7)/2))
    {
      last_added = 0;
      memUltimatic = FALSE;
    }
  }
/************************************************************************************************************/

/*************************************** Check if both DIT and DAH are pressed ******************************/
  Iambic();
/************************************************************************************************************/

/*************************************** Handle if either DIT or DAH are pressed ****************************/
  if(!(PaddleState & 2) && dit && !dah)
  { // Dit is pressed after it has been released
    addDit(PaddleMemoryLength++);
    iambic = FALSE;
    memUltimatic = FALSE;
  }
  // Save dah status for potential multi-keying
  if(!(PaddleState & 8) && dah && !dit)
  { // Dah is pressed after it has been released
	addDah(PaddleMemoryLength++);
    iambic = FALSE;
    memUltimatic = FALSE;
  }
/************************************************************************************************************/
}

void addDit(byte pos)
{
  PaddleMemory[pos] = 'E';		// E = dit pressed alone
  PaddleMemory[pos+1] = 0;

  // Are we still within dah- or ditpause?
  if(PaddleState)
  {
    if((PaddleState & 12))
    { // Keying DIT after DAH within dahpause
      // In order to respect the dahpause, we
      // add the length of a dah + the length of a pause
      ditTimer = dahTimer + ((ditlength*4)/1000);
      // Add (1 -3) ditlengths to dahTimer
      // 1 = length of a dah (which will actually not be keyed here)
      // 3 = length of a dit
      // we need this do calculate the next ditpause
      // (which actually comes after the DAH)
      dahTimer = ditTimer - ((ditlength*2)/1000);
    }
    else
    { // Keying DIT after DIT within ditpause
      // In order to respect the dahpause, we
      // add the length of a dit + the length of a pause
      ditTimer = ditTimer + ((ditlength*2)/1000);
    }
  }
  else
  {
    ditTimer = millis();
  }

  last_added = 'E';
  PaddleState |= 2; // Block dit
}

void addDah(byte pos)
{
  PaddleMemory[pos] = 'T';		// T = dah pressed alone
  PaddleMemory[pos+1] = 0;

  // Are we still within dah- or ditpause ?
  if(PaddleState)
  {
    if((PaddleState & 3)){
      // Keying DAH after DIT within ditpause
      // In order to respect the ditpause, we
      // add the length of a dit + the length of a pause
      dahTimer = ditTimer + ((ditlength*2)/1000);
      // Add (3 - 1) ditlengths to ditTimer
      // 3 = length of a dah
      // 1 = length of a dit (which will actually not be keyed here)
      // we need this do calculate the next ditpause
      // (which actually comes after the DAH)
      ditTimer = dahTimer + ((ditlength*2)/1000);
    }
    else
    { // Keying DAH after DAH within dahpause
      // In order to respect the dahpause, we
      // add the length of a dah + the length of a pause
      dahTimer = dahTimer + ((ditlength*4)/1000);
    }
  }
  else
  {
    dahTimer = millis();
  }

  last_added = 'T';
  PaddleState |= 8; // Block dah
}

void Iambic()
{
  if(dit && dah)
  {
    if(PaddleMemoryLength > 0)
    {
      if(PaddleMemory[PaddleMemoryLength - 1] == 'E' && !(PaddleState & 2))
      {
        if(mode == SINGLELEVER || (mode == ULTIMATIC && memUltimatic))
          addDit(PaddleMemoryLength++);
        else // IAMBIC_A + IAMBIC_B + first change in ULTIMATIC
        {
          addDah(PaddleMemoryLength++);
          memUltimatic = TRUE;
        }
      }
      else if(PaddleMemory[PaddleMemoryLength - 1] == 'T' && !(PaddleState & 8))
      {
        if(mode == SINGLELEVER || (mode == ULTIMATIC && memUltimatic))
          addDah(PaddleMemoryLength++);
        else // IAMBIC_A + IAMBIC_B + first change in ULTIMATIC
        {
          addDit(PaddleMemoryLength++);
          memUltimatic = TRUE;
        }
      }
    }
    else if(last_added == 'E' && !(PaddleState & 2))
    {
      if(mode == SINGLELEVER || (mode == ULTIMATIC && memUltimatic))
        addDit(0);
      else // IAMBIC_A + IAMBIC_B + first change in ULTIMATIC
      {
        addDah(0);
        memUltimatic = TRUE;
      }
      PaddleMemoryLength = 1;
    }
    else if(last_added == 'T' && !(PaddleState & 8))
    {
      if(mode == SINGLELEVER || (mode == ULTIMATIC && memUltimatic))
        addDah(0);
      else // IAMBIC_A + IAMBIC_B + first change in ULTIMATIC
      {
        addDit(0);
        memUltimatic = TRUE;
      }
      PaddleMemoryLength = 1;
    }
	iambic = TRUE;
  }

  // Special case for IAMBIC_B: Are we going to give the opposite character?
  if(mode == IAMBIC_B && !(dit && dah) && iambic)
  {
    if(last_added == 'T')
      addDit(PaddleMemoryLength++);
    else
      addDah(PaddleMemoryLength++);
    iambic = FALSE;
  }
}

byte QueryPaddleMemory()
{
  if(PaddleMemoryLength > 0)
    return PaddleMemory[0];
  else
    // finished keying
    return 0;
}

void MovePaddleMemory()
{
  if(PaddleMemoryLength > 0)
    // Advance one step in the PaddleMemory
    // Actually shift it towards left (remove the very first letter)
    memmove(PaddleMemory, PaddleMemory + 1, PaddleMemoryLength--);
}

void Paddle()
{
  byte memory = 0;
  if(state == NONE && !sendMem && !sendWinkey)
  { // Check if we want to send something
    memory = QueryPaddleMemory();

    if(memory == 'E') // DIT
    {
LABEL_DIT:
      if(config)
        key_trx = FALSE;
      else
        key_trx = TRUE;

      tx();
      time = micros();

      if(!sendBuffer && (!config || (configFlag == recMem && memoryNr > 0) || configFlag == recCall))
        keyedsomething = TRUE;

      // Add dit to ditdahbuffer for later character evaluation
      ditdahbuffer[ditdahcounter++] = 'E';
      // Remember that we are currently sending DIT
      state = DIT;
    }
    // Check if we want to send DAH
    else if(memory == 'T')
    {
LABEL_DAH:
      if(config)
        key_trx = FALSE;
      else
        key_trx = TRUE;

      tx();
      time = micros();

      if(!sendBuffer && (!config || (configFlag == recMem && memoryNr > 0) || configFlag == recCall))
        keyedsomething = TRUE;
      // Add dah to ditdahbuffer for later character evaluation
      ditdahbuffer[ditdahcounter++] = 'T';
      // Remember that we are currently sending DAH
      state = DAH;
    }
  }

  // Check if we are (already) sending DIT
  if(state == DIT){
    // Finished sending DIT?
    if((micros()-time) >= ditlength)
    {
      rx();
      time = micros();
      // Now we have to wait a little bit before keying again
      state = DITPAUSE;
      MovePaddleMemory();
    }
  }
  // Check if we are (already) sending DAH
  if(state == DAH){
    // Finished sending DAH?
    if((micros()-time) >= 3*ditlength)
    {
      rx();
      time = micros();
      // Now we have to wait a little bit before keying again
      state = DAHPAUSE;
      MovePaddleMemory();
    }
  }

  // Wait after sending a dit
  if(state == DITPAUSE || state == DAHPAUSE){
    if((micros()-time) >= ditlength)
    { // If we are sending the buffer than we can directly quit here without waiting for any possible additional user input
      // This would otherwise effect the timing quite severely
      if(sendBuffer)
      {
        state = EVALUATECHAR;
      }
      else
      {
        memory = QueryPaddleMemory();

        switch(memory)
        {
        case 'T':
          goto LABEL_DAH;
          break;

        case 'E':
          goto LABEL_DIT;
          break;
        }

        // If we are not sending anything anymore we can evaluate the character
        if((micros()-time) >= toleranceTime)
        {
          state = EVALUATECHAR;
        }
        //else
        // Do not do anything here, just wait if
        // within one additional ditlength:
        // a) another dit is keyed
        // b) another dah is keyed
        // c) nothing happened (-> evaluate character)
      }
    }
  }
}
