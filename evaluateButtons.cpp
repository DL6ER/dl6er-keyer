#include "routines.h"

int val_old = 0;

/*
	Determined Values:
            10 bit / calculated 10bit value
	MENU -> 0	   / 0

	SW 1 -> 835    / 835
	  R1=1.0/(1.0/4.7 + 1.0/2.2)
	  (6.6/(R1+6.6)) * 1024

	SW 2 -> 678    / 675
	  R1=1.0/(1.0/4.7 + 1.0/4.4)
      (4.4/(R1+4.4)) * 1024

	SW 3 -> 454    / 456
      R1=1.0/(1.0/4.7 + 1.0/6.6)
      (2.2/(R1+2.2)) * 1024
*/
// I know this is ugly, but actually it is the best way to do this!

// Measured values with 2k2 resistors
#define MEM9		827
#define MEM8		807
#define MEM7		784
#define MEM6		754
#define MEM5		716
#define MEM4		666
#define MEM3		597
#define MEM2		494
#define MEM1		324
#define UNCERTAINTY	10
#define MEM1UPPER	MEM1+UNCERTAINTY
#define MEM1LOWER	MEM1-UNCERTAINTY
#define MEM2UPPER	MEM2+UNCERTAINTY
#define MEM2LOWER	MEM2-UNCERTAINTY
#define MEM3UPPER	MEM3+UNCERTAINTY
#define MEM3LOWER	MEM3-UNCERTAINTY
#define MEM4UPPER	MEM4+UNCERTAINTY
#define MEM4LOWER	MEM4-UNCERTAINTY
#define MEM5UPPER	MEM5+UNCERTAINTY
#define MEM5LOWER	MEM5-UNCERTAINTY
#define MEM6UPPER	MEM6+UNCERTAINTY
#define MEM6LOWER	MEM6-UNCERTAINTY
#define MEM7UPPER	MEM7+UNCERTAINTY
#define MEM7LOWER	MEM7-UNCERTAINTY
#define MEM8UPPER	MEM8+UNCERTAINTY
#define MEM8LOWER	MEM8-UNCERTAINTY
#define MEM9UPPER	MEM9+UNCERTAINTY
#define MEM9LOWER	MEM9-UNCERTAINTY

int debounceAnalog(byte nr, int count);
void Memory(byte nr);

void evaluateButtons()
{
  // Reads the value from the specified analog pin.
  // 10-bit analog to digital converter.
  // This yields a resolution between readings of: 5 volts / 1024 units or, .0049 volts (4.9 mV) per unit.
  int val = debounceAnalog(CONN_SWITCH, 10);

  // Check if something has changed
  if(val_old > 0 && abs(val_old - val) > 10)
  {
    if(val > MEM1LOWER && val < MEM1UPPER)
    {
      Memory(0);
    }
    else if(val > MEM2LOWER && val < MEM2UPPER)
    {
      Memory(1);
    }
    else if(val > MEM3LOWER && val < MEM3UPPER)
    {
      Memory(2);
    }
    else if(val > MEM4LOWER && val < MEM4UPPER)
    {
      Memory(3);
    }
    else if(val > MEM5LOWER && val < MEM5UPPER)
    {
      Memory(4);
    }
    else if(val > MEM6LOWER && val < MEM6UPPER)
    {
      Memory(5);
    }
    else if(val > MEM7LOWER && val < MEM7UPPER)
    {
      Memory(6);
    }
    else if(val > MEM8LOWER && val < MEM8UPPER)
    {
      Memory(7);
    }
    else if(val > MEM9LOWER && val < MEM9UPPER)
    {
      Memory(8);
    }
// For measuring / debugging purposes:
/*    Keyboard.print(val);
        Keyboard.print(" ");
    Keyboard.print(abs(val_old - val));
        Keyboard.print(" ");
    Keyboard.println(val_old);
*/
  }
  // Save current value
  val_old = val;
}

void Memory(byte nr)
{
  if(configFlag == recMem)
  { // Are we recording yet?
    if(memoryNr == 0)
    { // Initiate recording to this memory
      memoryNr = nr + 1;
      char buffer[4];
      itoa(memoryNr,buffer,10);
      buffer[1] = ' ';
      buffer[2] = 'K';
      buffer[3] = 0;
      sendConfigString(buffer);
    }
    else
      // Skip, if we have already decided which memory to use
      return;
  }
  else
    // If not recording, then send the according memory
	sendMemory(nr);
}

int debounceAnalog(byte nr, int count)
{
  long val = 0, val2 = 0;
  for(int i = 0; i < count; i++)
    val += analogRead(nr);

LABEL_AGAIN:
  for(int i = 0; i < 2*count; i++)
    val2 += analogRead(nr);

  if(abs(val-val2) < (long)(2*count))
  { // Accept this value
    return (int)(val2 / 2 / count);
  }
  else
  { // Too much fluctuation, do it again
    val = val2;
    val2 = 0;
    goto LABEL_AGAIN;
  }
}

//****** Code below was contributed by Stojan, DK4SR *******//

#if defined(USE_ROTARY_ENCODER)
// Rotary Encoder State Tables
#ifdef OPTION_ENCODER_HALF_STEP_MODE
  // Use the half-step state table (emits a code at 00 and 11)
  const unsigned char ttable[6][4] = {
    {0x3 , 0x2, 0x1,  0x0}, {0x23, 0x0, 0x1, 0x0},
    {0x13, 0x2, 0x0,  0x0}, {0x3 , 0x5, 0x4, 0x0},
    {0x3 , 0x3, 0x4, 0x10}, {0x3 , 0x5, 0x3, 0x20}
  };
#else
  // Use the full-step state table (emits a code at 00 only)
  const unsigned char ttable[7][4] = {
    {0x0, 0x2, 0x4,  0x0}, {0x3, 0x0, 0x1, 0x10},
    {0x3, 0x2, 0x0,  0x0}, {0x3, 0x2, 0x1,  0x0},
    {0x6, 0x0, 0x4,  0x0}, {0x6, 0x5, 0x0, 0x10},
    {0x6, 0x5, 0x4,  0x0},
  };
#endif
  unsigned char state1 = 0;
#define DIR_CCW 0x10 // CW Encoder Code
#define DIR_CW 0x20  // CCW Encoder Code
#endif

void check_rotary_encoder()
{
#if defined(USE_ROTARY_ENCODER)
  static unsigned long timestamp[5];

  unsigned char pinstate = (digitalRead(rotary_pin2) << 1) | digitalRead(rotary_pin1);
  state1 = ttable[state1 & 0xf][pinstate];
  unsigned char result = (state1 & 0x30);

  if (result) {                                    // If rotary encoder modified
    timestamp[0] = timestamp[1];                   // Encoder step timer
    timestamp[1] = timestamp[2];
    timestamp[2] = timestamp[3];
    timestamp[3] = timestamp[4];
    timestamp[4] = millis();

    // Encoder step time difference for 10's step
    unsigned long elapsed_time = (timestamp[4] - timestamp[0]);

    if (result == DIR_CW) {
      if (elapsed_time < 250) {changeWpM(2);} else {changeWpM(1);};
    }
    if (result == DIR_CCW) {
      if (elapsed_time < 250) {changeWpM(-2);} else {changeWpM(-1);};
    }

  } // if (result)
#endif
}
