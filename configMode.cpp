#include "routines.h"

// Allow configuration of the keyer, e.g. setting WpM or starting memory keying
void evaluateConfigMode()
{ // Switch according to character keyed
  char buffer[4];

  if(menuswitch)
  { // Change WpM speed if keyed dit or dah
    if(configBuffer == 't')
    { // Decrease by 1
      time = micros();
      changeWpM(-1);
      clearSendbuffer();
      sendConfigString("V");
    }
    else if(configBuffer == 'e')
    { // Increase by 1
      time = micros();
      changeWpM(1);
      clearSendbuffer();
      sendConfigString("V");
    }
    else
    {
      return;
    }
    configBuffer = 0;
  }

  if(configFlag == setWPM)
  {
    if(configBuffer == 'e')
    { // Increase WpM: Paddle-Dit
      changeWpM(1);
      clearSendbuffer();
      sendConfigString("V");
      time = micros();
    }
    else if(configBuffer == 't')
    { // Decrease WpM: Paddle-Dit
      changeWpM(-1);
      clearSendbuffer();
      sendConfigString("V");
      time = micros();
    }
    // Whatever it was -> reset
    configBuffer = 0;
    return;
  }
  else if(configFlag == setKeyboardMode)
  {
    if(configBuffer == 't')
    { // nothing
      useKeyboard = 0;
      configFlag = NONE;
    }
    else if(configBuffer == 'p')
    { // only paddle
      useKeyboard = 1;
      configFlag = NONE;
    }
    else if(configBuffer == 'm')
    { // paddle + memory
      useKeyboard = 2;
      configFlag = NONE;
    }
    else if(configBuffer == 'a')
    {
	  useKeyboard = 3;
      configFlag = NONE;
    }
    if(configFlag != setKeyboardMode)
    {
      leaveConfigModeWithK();
 //     time = micros();
    }
    // Whatever it was -> reset
    configBuffer = 0;
    return;
  }
  else if(configFlag == setFarnsworth)
  { // Try to convert configBuffer into a number
    byte newFarnsworth = configBuffer - '0';
    if(newFarnsworth >= 0 && newFarnsworth <= 9)
    {
      Farnsworth = newFarnsworth;
      configFlag = NONE;
    }
    else if(configBuffer == 't')
    {
      Farnsworth = 0;
      configFlag = NONE;
    }
    else if(configBuffer == 'e')
    {
      Farnsworth = 1;
      configFlag = NONE;
    }
    if(configFlag != setFarnsworth)
    {
      // Recalculate timing
      changeWpM(0);
      itoa(Farnsworth, buffer, 10);
      sendConfigString("F ");
      sendConfigString(buffer);
      sendConfigString(" ");
//      sendConfigString("K");
      leaveConfigModeWithK();

//      time = micros();
    }
    // Whatever it was -> reset
    configBuffer = 0;
    return;
  }
  else if(configFlag == setTolerance)
  { // Try to convert configBuffer into a number
    byte newTolerance = configBuffer - '0';
    if(newTolerance >= 0 && newTolerance <= 8)
    {
      charTolerance = newTolerance;
      configFlag = NONE;
    }
    else if(configBuffer == 't')
    {
      charTolerance = 0;
      configFlag = NONE;
    }
    else if(configBuffer == 'e')
    {
      charTolerance = 1;
      configFlag = NONE;
    }
    else if(configBuffer == 'r')
    { // Reset to default
      charTolerance = 4;
      configFlag = NONE;
    }
    if(configFlag != setTolerance)
    {
      // Recalculate timing
      changeWpM(0);
      itoa(charTolerance, buffer, 10);
      sendConfigString("J ");
      sendConfigString(buffer);
      sendConfigString(" ");
//      sendConfigString("K");
      leaveConfigModeWithK();

//      time = micros();
    }
    // Whatever it was -> reset
    configBuffer = 0;
    return;
  }
  else if(configFlag == setTailTime)
  { // Try to convert configBuffer into a number
    byte nr = configBuffer - '0';
    if(nr >= 0 && nr <= 9)
    {
      pttTailTime = nr;
      configFlag = NONE;
    }
    else if(configBuffer == 't')
    {
      pttTailTime = 0;
      configFlag = NONE;
    }
    else if(configBuffer == 'e')
    {
      pttTailTime = 10;
      configFlag = NONE;
    }
    else if(configBuffer == 'n')
    {
      pttTailTime = 90;
      configFlag = NONE;
    }
    // Whatever it was -> reset
    configBuffer = 0;
    // Has it been set?
    if(configFlag != setTailTime)
    {
      sendConfigString("G ");
      itoa(10*pttTailTime, buffer, 10);
      shortFigures = TRUE;
      sendConfigString(buffer);
      shortFigures = FALSE;
      sendConfigString(" ");
//      sendConfigString("K");
      leaveConfigModeWithK();

//      time = micros();
    }
    return;
  }
  else if(configFlag == setHangTime)
  { // Try to convert configBuffer into a number
    byte nr = configBuffer - '0';
    if(nr >= 0 && nr <= 9)
    {
      pttHangTime = nr;
      configFlag = NONE;
    }
    else if(configBuffer == 't')
    {
      pttHangTime = 0;
      configFlag = NONE;
    }
    else if(configBuffer == 'e')
    {
      pttHangTime = 1;
      configFlag = NONE;
    }
    else if(configBuffer == 'i')
    {
      pttHangTime = 3;
      configFlag = NONE;
    }
    else if(configBuffer == 'n')
    {
      pttHangTime = 9;
      configFlag = NONE;
    }
    else if(configBuffer == 's')
    {
      pttHangTime = 18;
      configFlag = NONE;
    }
    else if(configBuffer == 'h')
    {
      pttHangTime = 36;
      configFlag = NONE;
    }
    // Whatever it was -> reset
    configBuffer = 0;
    // Has it been set?
    if(configFlag != setHangTime)
    {
      itoa(pttHangTime, buffer, 10);
      shortFigures = TRUE;
      sendConfigString("H ");
      sendConfigString(buffer);
      sendConfigString(" ");
//      sendConfigString("K");
      leaveConfigModeWithK();
      shortFigures = FALSE;

//      time = micros();
    }
    return;
  }
  else if(configFlag == setLeadInTime)
  { // Try to convert configBuffer into a number
    byte nr = configBuffer - '0';
    if(nr >= 0 && nr <= 9)
    {
      pttLeadInTime = nr;
      configFlag = NONE;
    }
    else if(configBuffer == 'n')
    {
      pttLeadInTime = 90;
      configFlag = NONE;
    }
    else if(configBuffer == 't')
    {
      pttLeadInTime = 0;
      configFlag = NONE;
    }
    else if(configBuffer == 'e')
    {
      pttLeadInTime = 10;
      configFlag = NONE;
    }
    else if(configBuffer == 'i')
    {
      pttLeadInTime = 20;
      configFlag = NONE;
    }
    else if(configBuffer == 's')
    {
      pttLeadInTime = 30;
      configFlag = NONE;
    }
    else if(configBuffer == 'h')
    {
      pttLeadInTime = 40;
      configFlag = NONE;
    }
    // Whatever it was -> reset
    configBuffer = 0;
    // Has it been set?
    if(configFlag != setLeadInTime)
    {
      itoa(pttLeadInTime*10, buffer, 10);
      shortFigures = TRUE;
      sendConfigString("L ");
      sendConfigString(buffer);
      sendConfigString(" ");
//      sendConfigString("K");
      leaveConfigModeWithK();
      shortFigures = FALSE;

//      time = micros();
    }
    return;
  }
  else if(configFlag == recMem)
  { // Are we already recording?
    if(memoryNr > 0)
    {
      if(configBuffer)
      {
        memoryBuffer[memoryCounter++] = configBuffer;
      }
      if(menuswitch)
      { // Abort recording
        memoryNr = 0;
        memoryCounter = 0;
        configFlag = NONE;
      }
    }
    else
    {
      // Try to convert configBuffer into a number
      byte nr = configBuffer - '0';
      if(nr >= 1 && nr <= 9)
      {
        memoryNr = nr;
      }
      else if(configBuffer == 'n')
      {
        memoryNr = 9;
      }
      else if(configBuffer == 't')
      {
        memoryNr = 2;
      }
      else if(configBuffer == 'e')
      {
        memoryNr = 1;
      }
      if(memoryNr > 0)
        sendConfigString("K");
    }
    // Whatever it was -> reset
    configBuffer = 0;
    return;
  }
  else if(configFlag == recCall)
  { // Are we already recording?
    if(configBuffer)
    {
      memoryBuffer[memoryCounter++] = configBuffer;
    }
    if(menuswitch)
    { // Abort recording
      memoryCounter = 0;
      configFlag = NONE;
    }
    // Whatever it was -> reset
    configBuffer = 0;
    return;
  }
  else if(configFlag == setBeaconInterval)
  { // Try to convert configBuffer into a number
    byte newBreak = configBuffer - '0';
    if(newBreak >= 0 && newBreak <= 9)
    {
      beaconInterval = newBreak;
      configFlag = NONE;
    }
    else if(configBuffer == 'n')
    {
      beaconInterval = 9;
      configFlag = NONE;
    }
    else if(configBuffer == 't')
    {
      beaconInterval = 0;
      configFlag = NONE;
    }
    else if(configBuffer == 'e')
    {
      beaconInterval = 1;
      configFlag = NONE;
    }
    else if(configBuffer == 'r')
    { // Reset to default
      beaconInterval = 4;
      configFlag = NONE;
    }
    if(configFlag != setBeaconInterval)
    {
      itoa(beaconInterval, buffer, 10);
      sendConfigString("D ");
      sendConfigString(buffer);
      sendConfigString(" ");
//      sendConfigString("K");
      leaveConfigModeWithK();

//      time = micros();
    }
    // Whatever it was -> reset
    configBuffer = 0;
    return;
  }
  else if(configFlag == PROGRAMM)
  {
    if(configBuffer == 'p')
    {
      programmingMode = TRUE;
      wdt_disable();
      config = FALSE;
    }
    // Whatever it was -> reset
    configBuffer = 0;
    return;
  }
  else if(configFlag == setSidetoneFreq)
  { // Try to convert configBuffer into a number
    int sidetoneTarget = configBuffer - '0';
    if(sidetoneTarget >= 2 && sidetoneTarget <= 9)
    {
      sidetoneFreq = 100 * sidetoneTarget;
      configFlag = NONE;
    }
    else if(configBuffer == 'n')
    {
      sidetoneFreq = 900;
      configFlag = NONE;
    }
    else if(configBuffer == 'r')
    { // Reset to default sidetone frequency
      sidetoneFreq = 700;
      configFlag = NONE;
    }
    // Has the sidetone freq been set?
    if(configFlag != setSidetoneFreq)
    {
      itoa(sidetoneFreq, buffer, 10);
      shortFigures = TRUE;
      sendConfigString("Z ");
      sendConfigString(buffer);
      sendConfigString(" ");
//      sendConfigString("K");
      leaveConfigModeWithK();
      shortFigures = FALSE;

//      time = micros();
    }
    // Whatever it was -> reset
    configBuffer = 0;
    return;

  }
  // Requested to change keying style?
  else if(configFlag == setKeyingMode)
  {
    switch(configBuffer)
    {
      case 'a':
        mode = IAMBIC_A;
        configFlag = NONE;
        sendConfigString("A");
      break;
      case 'b':
        mode = IAMBIC_B;
        configFlag = NONE;
        sendConfigString("B");
      break;
      case 'u':
        mode = ULTIMATIC;
        configFlag = NONE;
        sendConfigString("U");
      break;
      case 's':
        mode = SINGLELEVER;
        configFlag = NONE;
        sendConfigString("S");
      break;
      case 'g':
        mode = BUG;
        configFlag = NONE;
        sendConfigString("G");
      break;
      case 'k':
        mode = STRAIGHT;
        configFlag = NONE;
        sendConfigString("K");
        // No config-mode in straight-key mode
        config = FALSE;
      break;
    }
    // Has the keying mode been set?
    if(configFlag != setKeyingMode)
    {
      sendConfigString(" ");
//      sendConfigString("K");
      leaveConfigModeWithK();
//      time = micros();
    }
    // Whatever it was -> reset
    configBuffer = 0;
    return;
  }

  switch(configBuffer)
  {
  // Nothing has happened
  case 0x00:
  break;

  case 'e':
    // Send memory 0 (E) + leave the Config-Mode immediately
    sendMemory(0);
    ditdahcounter = 0;
  break;

  case 't':
    // Send memory 1 (T) + leave the Config-Mode immediately
    sendMemory(1);
    ditdahcounter = 0;
  break;
/*
  case 'q': // Query info
    sendConfigString(GET(MYCALL));
    itoa(WpM, buffer, 10);
    sendConfigString("  WPM ");
    shortFigures = TRUE;
    sendConfigString(buffer);
    shortFigures = FALSE;

    sendConfigString("  MODE ");
    switch(mode)
    {
      case IAMBIC_A:
      sendConfigString("A");
      break;
      case IAMBIC_B:
      sendConfigString("B");
      break;
      case ULTIMATIC:
      sendConfigString("U");
      break;
      case SINGLELEVER:
      sendConfigString("S");
      break;
      case BUG:
      sendConfigString("B");
      break;
      case STRAIGHT:
      sendConfigString("S");
      break;
    }
    itoa(key_output, buffer, 10);
    sendConfigString("  O ");
    sendConfigString(buffer);

    sendConfigString("  PC ");
    if(useWinkeyEmulation)
      sendConfigString("On");
    else
      sendConfigString("Off");

    sendConfigString("K");
    itoa(sidetoneFreq, buffer, 10);
    sendConfigString("  SF ");
    shortFigures = TRUE;
    sendConfigString(buffer);
    shortFigures = FALSE;

    time = micros();
  break;
*/
  case 'k': // Set keying mode
    sendConfigString("K?");
#if USELCD == TRUE
    LCD.Row("Keyboard mode");
#endif
    configFlag = setKeyingMode;

    time = micros();
  break;

  case 'z': // Set sidetonefrequency (x * 100Hz)
    sendConfigString("Z?");
#if USELCD == TRUE
    LCD.Row("Sidetone");
#endif
    configFlag = setSidetoneFreq;

    time = micros();
  break;

  case 'a': // Toggle sidetone On/Off (always active in Config-Mode)
    if(sidetoneActive)
    {
      sidetoneActive = FALSE;
#if USELCD == TRUE
    LCD.Row("Sidetone OFF");
#endif
      sendConfigString("A OFF");
    }
    else
    {
      sidetoneActive = TRUE;
#if USELCD == TRUE
    LCD.Row("Sidetone ON");
#endif
      sendConfigString("A ON");
    }

    time = micros();
  break;

  case 'u': // Activate Tune-Mode (key TRX and leave config-mode)
            // Can be interrupted with any kind of keying (also via PC)
    config = FALSE;
    clearSendbuffer();
    key_trx = TRUE;
    tx();
#if USELCD == TRUE
    LCD.Row("Tune");
#endif
  break;

  case 'x': // Toggle dit/dah inputs from the paddle
    sendConfigString("X ");
    if(toggleDitDah)
    {
      toggleDitDah = FALSE;
#if USELCD == TRUE
    LCD.Row("DIT / DAH");
#endif
      sendConfigString("ET");
    }
    else
    {
      toggleDitDah = TRUE;
#if USELCD == TRUE
    LCD.Row("DAH / DIT");
#endif
      sendConfigString("TE");
    }
    time = micros();
  break;

  case 'p': // Programming mode (has to be confirmed afterwards)
    sendConfigString("P?");
    configFlag = PROGRAMM;
/*    sendConfigString("P K");
    programmingMode = TRUE;
    wdt_disable(); */
  break;

  case 'w': // Change WpM speed
    sendConfigString("W?");
    configFlag = setWPM;
#if USELCD == TRUE
    LCD.Row("Set WpM");
#endif

    time = micros();
  break;

#if DEBUG == TRUE
  case 'd': // DEBUG: How much RAM is still available on the chip?
    Keyboard.println(freeRAM());

    time = micros();
  break;
#endif

  case 'o': // Toggle KEY/PTT output
    sendConfigString("O ");
    if(key_output == 1)
    {
      key_output = 2;
      sendConfigString("2");
#if USELCD == TRUE
    LCD.Row("Selecting TRX 2");
    LCD.Status();
#endif
    }
    else
    {
      key_output = 1;
      sendConfigString("1");
#if USELCD == TRUE
    LCD.Row("Selecting TRX 1");
    LCD.Status();
#endif
    }

    time = micros();
  break;

  case 'y': // Toggle keyboard character evaluation On/Off
    sendConfigString("Y?");
    configFlag = setKeyboardMode;
#if USELCD == TRUE
    LCD.Row("Keyboard Mode");
#endif

    time = micros();
  break;

  case 'm': // Mute TX/PTT lines On/Off
    sendConfigString("M ");
    if(TRXMute)
    {
      TRXMute = FALSE;
#if USELCD == TRUE
    LCD.Row("Unmute TRX");
#endif
      sendConfigString("OFF");
    }
    else
    {
      TRXMute = TRUE;
#if USELCD == TRUE
    LCD.Row("Mute TRX");
#endif
      sendConfigString("ON");
    }

    time = micros();
  break;

  case 'i': // Invert TX line Active state (normally HIGH == ACTIVE)
    sendConfigString("I ");
    if(TRXActive == HIGH)
    {
      TRXActive = LOW;
#if USELCD == TRUE
    LCD.Row("LOW = ACTIVE");
#endif
      sendConfigString("TE");
    }
    else
    {
      TRXActive = HIGH;
#if USELCD == TRUE
    LCD.Row("HIGH = ACTIVE");
#endif
      sendConfigString("ET");
    }

    time = micros();
  break;

  case 'j': // Set character tolerance
    sendConfigString("J?");
    configFlag = setTolerance;
#if USELCD == TRUE
    LCD.Row("Set tolerance");
#endif

    time = micros();
  break;

  case 's': // Save settings into EEPROM
    writeSettingsToEEPROM();
    sendConfigString("S K");
#if USELCD == TRUE
    LCD.Row("Settings saved");
#endif

    time = micros();
  break;

  case 'b': // Start beacon mode
    // Send memory 0 + set beaconMode TRUE
    beaconMode = TRUE;
#if USELCD == TRUE
    LCD.Status();
#endif
    sendMemory(0);
  break;

  case 'd': // Set beacon interval
    sendConfigString("D?");
    configFlag = setBeaconInterval;
#if USELCD == TRUE
    LCD.Row("Beacon interval");
#endif

    time = micros();
  break;

  case 'n': // Set keyboard autospace feature
    sendConfigString("N ");
    if(keyboardAutospace)
    {
      keyboardAutospace = FALSE;
#if USELCD == TRUE
    LCD.Row("Autospace OFF");
#endif
      sendConfigString("OFF");
    }
    else
    {
      keyboardAutospace = TRUE;
#if USELCD == TRUE
    LCD.Row("Autospace ON");
#endif
      sendConfigString("ON");
    }

    time = micros();
  break;

  case 'r': // Record memory
    sendConfigString("R nr?");
#if USELCD == TRUE
    LCD.Row("Record MEM Nr?");
#endif
    memoryCounter = 0;
    memoryNr = 0;
    configFlag = recMem;

    time = micros();
  break;

  case 'l': // Set PTT lead-in time
    sendConfigString("L?");
#if USELCD == TRUE
    LCD.Row("LeadIn Time");
#endif
    configFlag = setLeadInTime;

    time = micros();
  break;

  case 'h': // Set PTT hang time
    sendConfigString("H?");
#if USELCD == TRUE
    LCD.Row("Hang Time");
#endif
    configFlag = setHangTime;

    time = micros();
  break;

  case 'g': // Set PTT tail time
    sendConfigString("G?");
#if USELCD == TRUE
    LCD.Row("Tail time");
#endif
    configFlag = setTailTime;

    time = micros();
  break;

  case 'f': // Set Farnsworth
    sendConfigString("F?");
#if USELCD == TRUE
    LCD.Row("Farnsworth mode");
#endif
    configFlag = setFarnsworth;

    time = micros();
  break;

  case 'c': // Record callsign
    sendConfigString("C?");
#if USELCD == TRUE
    LCD.Row("Record callsign");
#endif
    memoryCounter = 0;
    configFlag = recCall;

    time = micros();
  break;

#if USELCD == TRUE
  case 'v': // Reinitialize display
	LCD.init();
	LCD.ShowWpM();
	LCD.Status();
  break;
#endif

  case '1': // Send memory 1
    sendMemory(0);
  break;

  case '2': // Send memory 2
    sendMemory(1);
  break;

  case '3': // Send memory 3
    sendMemory(2);
  break;

  case '4': // Send memory 4
    sendMemory(3);
  break;

  case '5': // Send memory 5
    sendMemory(4);
  break;

  case '6': // Send memory 6
    sendMemory(5);
  break;

  case '7': // Send memory 7
    sendMemory(6);
  break;

  case '8': // Send memory 8
    sendMemory(7);
  break;

  case '9': // Send memory 9
    sendMemory(8);
  break;

  default: // Not recognized
    //time = micros();
    sendString("?");
  break;
  }

  // Whatever it was -> reset
  configBuffer = 0;
}

void leaveConfigModeWithK()
{
	sendConfigString("K");
	config = FALSE;
}
// Change WpM-Setting and recalculate timing
void changeWpM(int change)
{ // Check boundary
  if((WpM + change) >= MINWPM && (WpM + change) <= MAXWPM)
  {
    WpM += change;
    ditlength = 1200000 / WpM;
    ditlength_pause = (1200000 / (WpM * (10 - Farnsworth))) * 10;
#if USELCD == TRUE
	LCD.ShowWpM();
#endif
  }
  toleranceTime = (unsigned long)charTolerance*ditlength/4 + ditlength;
}

// Change WpM-Setting and recalculate timing
void setWpM(int value)
{ // Check boundary
  if(value >= MINWPM && value <= MAXWPM)
  {
    WpM = value;
    ditlength = 1200000 / WpM;
    ditlength_pause = (1200000 / (WpM * (10 - Farnsworth))) * 10;
#if USELCD == TRUE
	LCD.ShowWpM();
#endif
  }
  toleranceTime = (unsigned long)charTolerance*ditlength/4 + ditlength;
}

int freeRAM () {
  extern int __heap_start, *__brkval;
  int v;
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
}
