#include "definitions.h"

#include <avr/wdt.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <Arduino.h>

#include <Keyboard.h>
#if defined(KEY_LEFT_CTRL)
#define UseKeyboard
#endif

// print() will be redirected to write() through the Print class
class KB_ : public Print
{
public:
	virtual size_t write(uint8_t k);
	virtual size_t press(uint8_t k);
	virtual size_t release(uint8_t k);
	virtual void releaseAll(void);
};

#if USELCD == TRUE
class LCD_
{
public:
	virtual void ActivateBL();
	virtual void ShowWpM();
	virtual void Status();
	virtual void Row(char*);
	virtual void print(char);
	virtual void printHEX(unsigned char);
	virtual void init();
};
#endif

#if MAIN == 1
#include "morsecode.h"
#include "globalvars_init.h"
#else
#include "globalvars.h"
#endif

#define MAX(a, b) ((a) < (b) ? (b) : (a))

// Is bit == TRUE ?
#define TESTBIT(x, b) (x & b)
// Set bit = TRUE
#define SETBIT(x, b) x |= b
// Set bit = FALSE
#define CLEARBIT(x, b) x &= ~b

// Paddle.cpp
void RecordPaddleMemory();
void Paddle();

// Bug.cpp
void Bug();

// Straight.cpp
void Straight();

// TRX.cpp
void tx();
void rx();
void key_on();
void key_off();
void ptt_on();
void ptt_off();

// configMode.cpp
void evaluateConfigMode();
void changeWpM(int change);
void setWpM(int change);
int freeRAM();
void leaveConfigModeWithK();

// evaluateBuffer.cpp
void evaluateBuffer();
void sendString(char const *string);
void sendConfigString(char const * string);
void sendCharacter(char *letter);
void clearSendbuffer();
void addnextChar();

// WinkeyEmulation.cpp
void evaluateWinkey();
void wait_for_serial_rx();
void evaluateWinkeyModebyte(char serial_buffer);

// evaluateCharacter.cpp
void evaluateCharacter();
void clearDitDahBuffer();

// keycodes.cpp
char correctKeycode(char const *USKey);

// relays.cpp
#if USERELAYS == TRUE
void RelayInit();
void RelaySet(byte dataH, byte dataL);
void RelaySingle(byte nr, byte status);
#endif

// EEPROM.cpp
void writeSettingsToEEPROM();
int readSettingsFromEEPROM();
void initCallsign();
void writeCallsign();
char sendCallsign();

// Memory.cpp
int sendMemory(byte nr);
void initMemory(byte nr);
int writeMemory(byte nr);

// evaluateButtons.cpp
void evaluateButtons();
int debounceAnalog(byte nr, int count);
void check_rotary_encoder();
