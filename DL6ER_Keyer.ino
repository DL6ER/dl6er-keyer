/*
 Keyer by Nick, DL6ER

 Most important features:
 - Iambic A/B Keyer with full "squeezeability"
 - Ultimatic-Mode, Single-Lever emulation, ElBug emulation,
   Straight key support, ...
 - KeyerKeyboard -> Prints your characters on the computer as if they
   would have been typed in using an ordinary keyboard: Works with all
   characters that are defined in the international morse alphabet
   including German Umlauts, plus:
   - EEEEEE.... = [Backspace] (remove character left of the cursor)
   - TTTTTT.... = [Space]
   - TETETET... = [Return] (new line)
   - ETETETE... = [Shift] (next letter capital, automatically reset)
   - some more
 - Config-Mode supporting:
   - Change of speed
   - Memory sending (two preprogrammed memories)
   - and a lot more...
 - Winkey 1/2 Emulation (1200baud, fully compatible with e.g. UcxLog)

 Please review the documentation or the complete source code for
 a full feature description.
 */

#define MAIN 1
#include "routines.h"

#if USELCD == TRUE
#include <LiquidCrystal.h>
LiquidCrystal lcd(DISPLAY_RS, DISPLAY_EN, DISPLAY_D4, DISPLAY_D5, DISPLAY_D6, DISPLAY_D7);
#if USERELAYS == TRUE
#error Relays und display cannot be used together with Arduino Pro Micro
#endif
#endif

void setup() {
  // TX-KEY output
  pinMode(CONN_KEY1, OUTPUT);
  pinMode(CONN_KEY2, OUTPUT);
  digitalWrite(CONN_KEY1, LOW);
  digitalWrite(CONN_KEY2, LOW);
  // TX-PTT output
  pinMode(CONN_PTT1, OUTPUT);
  pinMode(CONN_PTT2, OUTPUT);
  digitalWrite(CONN_PTT1, LOW);
  digitalWrite(CONN_PTT2, LOW);
  // Paddle DIT input
  pinMode(CONN_DIT, INPUT_PULLUP);
  // Paddle DAH input
  pinMode(CONN_DAH, INPUT_PULLUP);
  // Configuration Switch input
  pinMode(CONN_SWITCH, INPUT);

#if USERELAYS == TRUE
  RelayInit();
#endif
#if USELCD == TRUE
  LCD.init();
//  lcd.print("DL6ER-Keyer");
#endif

#if defined(CONN_POTI)
  pinMode(CONN_POTI, INPUT);
#endif

#ifdef UseKeyboard
  Keyboard.begin();
#endif

  // Allow configuration of the pins to finish
  // so that we can safely read the inputs
  delay(10);

  rx();

  // Check if entering programming-mode (Config-switch pressed but DIT, DAH free?)
  if(!digitalRead(CONN_SWITCH) && digitalRead(CONN_DAH) && digitalRead(CONN_DIT))
    // Enter programmingMode
    programmingMode = TRUE;

#if WINKEYEMULATION == TRUE
    // Open serial port for WinkeyEmulation (1200baud, 8N1)
    Serial.begin(1200);
#endif

    // Allow the initialization to be finished before doing anything fancy
    // Note: May crash or simply hang without this
    delay(50);
    clearSendbuffer();

    // Check if buttons are pressed (Dit + Dah)
    if(!digitalRead(CONN_DIT) && !digitalRead(CONN_DAH))
    { // Reset to initial values
      // !!! DO NOT READ EEPROM !!!
      changeWpM(0);
      if(!digitalRead(CONN_SWITCH))
      {
        initMemory(0);
        initMemory(1);
        initMemory(2);
        initMemory(3);
        initMemory(4);
        writeSettingsToEEPROM();
      }
      delay(2000);
    }
    else
    { // Read settings from EEPROM
      if(!readSettingsFromEEPROM())
      { // If not successful use standard settings
        initMemory(0);
        initMemory(1);
        initMemory(2);
        initMemory(3);
        initMemory(4);
        initCallsign();
        writeSettingsToEEPROM();
      }
      delay(100);
      // Recalculate timing
      changeWpM(0);
#if USELCD == TRUE
      LCD.Status();
#endif
    }

#if STARTUPMSG == TRUE
    // Announce callsign + current WpM
    // Note: Can be interrupted at any time!
    sendConfigString("$ ");
    char buffer[3];
    shortFigures = TRUE;
    itoa(WpM, buffer, 10);
    sendConfigString(buffer);
    shortFigures = FALSE;
    sendConfigString(" K");
#else
    // We might have see a reset here.
    // Activate WinKey mode as we most
    // likely want to use it (reset might
    // have actually been induced by a
    // connecting software!)
    useWinkeyEmulation = TRUE;
#endif
  // Ready
}

void loop() {
  // Enable watchdog timer (1s)
  if(!programmingMode)
  {
    wdt_enable(WDTO_2S);
//    *(uint16_t *)0x0800 = 0x00;
    // Reset the WDT to avoid resets through the serial port
    wdt_reset();
  }

  // Read inputs
  if(!toggleDitDah)
  {
    dit = !digitalRead(CONN_DIT);
    dah = !digitalRead(CONN_DAH);
  }
  else
  {
    dah = !digitalRead(CONN_DIT);
    dit = !digitalRead(CONN_DAH);
  }
  menuswitch = (analogRead(CONN_SWITCH) < 10);

  // Some memory safety rules ...
  if(ditdahcounter > (MAXDITDAHLEN-1))
    ditdahcounter = 0;
  if(sendbufferlength > (MAXSENDBUFFERLEN-1))
    sendbufferlength = 0;

  // Keying = stop sending buffer
  if((dit || dah))
  { // However, but wait a little bit (100ms) after starting memory playback
    // so one has enough time to actually release the paddle
    if((!sendMem || (millis()-memTimer > 100)) && sendBuffer)
    {
      rx();
      clearSendbuffer();
      dit = dah = 0;
      // Do not directly start keying when interrupting buffer sending
      state = LETTERPAUSE;
    }

    if(useWinkeyEmulation && winkeyBUSY)
    { // Send BREAK-IN and IDLE afterwards (so that the software will not lock sending)
      Serial.write(0xC2);
      // Remove any possible winkeydata in the serial buffer
      while(Serial.read() != -1);
      Serial.write(0xC0);
      winkeyBUSY = FALSE;
      clearSendbuffer();
      rx();
      // Do not directly start keying when interrupting Winkey sending
      state = LETTERPAUSE;
    }
    if(beaconMode)
    {
      // Reset beaconMode if it is set
      beaconMode = FALSE;
#if USELCD == TRUE
      LCD.Status();
#endif
    }
  }

  RecordPaddleMemory();

  if(waiting_for_PTT)
    tx();

  // Paddle modes
  if(mode == IAMBIC_A || mode == IAMBIC_B || mode == ULTIMATIC || mode == SINGLELEVER || sendBuffer)
    Paddle();
  // Bug mode
  else if(mode == BUG)
    Bug();
  else if(mode == STRAIGHT)
    Straight();

  // If we are not sending anything anymore we can evaluate the character
  if(state == EVALUATECHAR)
  {
    // We only want to do this when we actually keyed the TRX (no status texts like Call + WpM)
    // In addition, we want to avoid this when sending from memory
    if((key_trx || config) && !sendBuffer)
    {
      state = LETTERPAUSE;
      evaluateCharacter();
      ditdahcounter = 0;
      clearDitDahBuffer();
    }
    // Without character evaluation we will simply go back to IDLE mode
    else
    {
      state = NONE;
      ditdahcounter = 0;
      clearDitDahBuffer();
    }
  }

  // Pause between letters (maybe longer than usual due to Farnsworth settings)
  if(state == LETTERPAUSE){
    if((micros()-time) >= 3*ditlength_pause)
    {
      state = NONE;
    }
  }

  // Pause between dit/dah (used by evaluateBuffer)
  if(state == INTRALETTERPAUSE){
    if((micros()-time) >= ditlength_pause)
    {
      state = NONE;
    }
  }

  //If we are at the last part of the message, check if there are additional characters in the charBuffer
  if(sendbufferlength < 3)
    addnextChar();

  // Send buffered text (dit/dah)
  if(sendbufferlength > 0 && state == NONE)
    evaluateBuffer();

  // Reset sendMem (so counter can begin to wait for next beacon keying)
  if(charBufferlength == 0 && sendbufferlength == 0 && sendMem)
  {
    sendMem = FALSE;
#if USELCD == TRUE
    LCD.Status();
#endif
  }

#if SENDMEMORYONLYDURINGMENU == FALSE
  // Evaluate possible memory keying buttons
  evaluateButtons();
#endif

  // Enter Config-Mode (No config mode in straight key mode!)
  if(config || menuswitch)
  {
    if(!config)
	{
      key_trx = FALSE;
      config = TRUE;
      beaconMode = FALSE;
      keyedsomething = FALSE;
      clearSendbuffer();
#if USELCD == TRUE
      LCD.Status();
#endif
      time = micros();
      // Announce Config-Mode
      sendbuffer[sendbufferlength++] = 0x01;
      sendbuffer[sendbufferlength++] = 'E';
      sendbuffer[sendbufferlength++] = 0x02;
    }

#if SENDMEMORYONLYDURINGMENU == TRUE
    // Evaluate possible memory keying buttons
    evaluateButtons();
#endif

    evaluateConfigMode();

    // Leave Config-Mode
    // due to timeout or due to memory sending
    unsigned long memoryTimeout = (50000000/WpM);
    if(memoryTimeout < 1000000) memoryTimeout = 1000000;
    if(!config || ((micros()-time) >= memoryTimeout && configFlag != recMem && configFlag != recCall) || ((micros()-time) >= memoryTimeout*2))
    { // Recorded memory?
      if(configFlag == recMem && memoryNr != 0 && memoryCounter > 0)
      {
        memoryBuffer[memoryCounter] = 0;
        writeMemory(memoryNr - 1); // internally begin counting from 0

        sendConfigString("R ");
        char buffer[2];
        // Spell out memory number
        itoa(memoryNr, buffer, 10);
        sendConfigString(buffer);
        sendConfigString(" K");
        memoryCounter = 0;
      }
      // Recorded callsign?
      if(configFlag == recCall && memoryCounter > 0)
      {
        memoryBuffer[memoryCounter] = 0;
        writeCallsign();

        sendConfigString("C ");
        sendConfigString(memoryBuffer);
        sendConfigString(" K");
        memoryCounter = 0;
      }
      // Reset config-options
      configFlag = NONE;
      if(config)
      {
        // Send "DIT DIT" when leaving because of timeout
        sendbuffer[sendbufferlength++] = 0x01;
        sendbuffer[sendbufferlength++] = 'E';
        sendbuffer[sendbufferlength++] = 'E';
        sendbuffer[sendbufferlength++] = 0x02;
        config = FALSE;
      }
#if USELCD == TRUE
      LCD.Status();
      LCD.Row("");
#endif
    }
  }

#if WINKEYEMULATION == TRUE
  // Check if data has arrived in the serial receive buffer (which holds up to 64 bytes)

  if(Serial.available())
    evaluateWinkey();

  if(winkeyBUSY && !sendWinkey && useWinkeyEmulation)
  { // Send IDLE only if idle more than 300mec
    if((millis() - winkeyIDLEtimer) > 300)
    {
      winkeyBUSY = FALSE;
      Serial.write(0xC0);
    }
  }
#endif

  // Beacon Mode
  if(beaconMode)
  {
    if(sendMem)
    {
      memTimer = millis();
    }
    else if((millis()-memTimer) >= 1000*beaconInterval)
    {
      sendMemory(0);
#if USELCD == TRUE
      LCD.Status();
      LCD.Row("");
#endif
    }
  }

  // Autospace for keyboard + memory recording
  if(keyboardAutospace && keyedsomething && (micros()-time) >= (((unsigned long)7* ditlength) + toleranceTime))
  {
    if(useKeyboard)
      KB.print(" ");

    if(config && configFlag == recMem)
      configBuffer = ' ';

#if USELCD == TRUE
      LCD.print(' ');
#endif

    keyedsomething = FALSE;
  }
  // pttTailTime = 10msec * pttTailTime
  // pttHangTime = ditlength * pttHangTime * 11 / 10 // Add 10% for safety reasons
  unsigned long pttTime = (unsigned long)10000 * pttTailTime + ditlength * ((pttHangTime * 11) / 10);
  // Do not disable PTT if:
  //  - Key is active
  //  - PTT is still not up yet (lead-in time)
  if(pttActive && ((micros()-keyOffTime) >= pttTime) && !keyActive && !waiting_for_PTT)
  {
    ptt_off();
  }

#if USELCD == TRUE && DISPLAY_BL != 0
  // Dim displaylight
  if(( (long)(display_timer+DISPLAY_TIMEOUT) < millis() ) && display_byte > 0)
  {
    display_byte = 255 - (millis() - (display_timer+DISPLAY_TIMEOUT))/2;
    analogWrite(DISPLAY_BL, display_byte);
  }
#endif

#if defined(CONN_POTI)
  // Query poti only when not in WinKey mode
  if(!useWinkeyEmulation)
  {
    if(potiByte > 250)
    {
      int poti = analogRead(CONN_POTI);
      float potiWpM = (float)poti/1024. * (POTI_WPM_MAX-POTI_WPM_MIN+1) + POTI_WPM_MIN;
      setWpM((int)potiWpM);
      potiByte = 0;
    }
    else
    {
      potiByte++;
    }
  }
#endif

#if defined(USE_ROTARY_ENCODER)
  check_rotary_encoder();
#endif

} //****************************** end loop ******************************//

// Evaluate keyboard events here, because this file knows if we have keyboard functionality available...
size_t KB_::press(uint8_t k)
{
  #ifdef UseKeyboard
  return Keyboard.press(k);
  #endif
}
size_t KB_::release(uint8_t k)
{
  #ifdef UseKeyboard
  return Keyboard.release(k);
  #endif
}
void KB_::releaseAll(void)
{
  #ifdef UseKeyboard
  Keyboard.releaseAll();
  #endif
}
size_t KB_::write(uint8_t c)
{
  #ifdef UseKeyboard
  return Keyboard.write(c);
  #endif
}

#if USELCD == TRUE
void LCD_::ActivateBL()
{
#if DISPLAY_BL != 0
	display_timer = millis();
	display_byte = 255;
    analogWrite(DISPLAY_BL,255);
#endif
}


void LCD_::ShowWpM()
{
  LCD.ActivateBL();
  lcd.setCursor(0,0);
  lcd.print("WpM ");
  char buffer[2];
  itoa(WpM, buffer, 10);
  lcd.print(buffer);
}
void LCD_::Status()
{
  char buffer[2];

  LCD.ActivateBL();

  // Set cursor position
  lcd.setCursor((LCD_WIDTH-8),0);

  if(sendMem)
  {
    lcd.print("MP");
    itoa(sendMem, buffer, 10);
    lcd.print(buffer);
  }
  else if(configFlag == recMem)
  {
    lcd.print("MR");
    if(!memoryNr)
      lcd.print("?");
    else
    {
      itoa(memoryNr, buffer, 10);
      lcd.print(buffer);
    }
  }
  else
    lcd.print("   ");

  if(beaconMode == TRUE)
    lcd.print("B");
  else
    lcd.print(" ");

  if(config == TRUE)
    lcd.print("M");
  else
    lcd.print(" ");

  if(useWinkeyEmulation == TRUE)
    lcd.print("PC");
  else
    lcd.print("  ");

  // Print selected output channel
  lcd.print(key_output);
}

void LCD_::Row(char * text)
{
  // Go to beginning of second row
  lcdcounter = 0;
  // Send each character to the scrolling routine
  for(byte i=0;i<strlen(text);i++)
    LCD.print(text[i]);
}

void LCD_::print(char text)
{
  byte i;
  if(text == 0)
    return;

  LCD.ActivateBL();

  if(text != 1){ // Normal character
    if(lcdcounter >= LCD_WIDTH)
    { // Remove first character from buffer
      memmove(lcdBuffer,lcdBuffer+1,LCD_WIDTH);
      lcdBuffer[LCD_WIDTH+1] = 0;
      lcdcounter = LCD_WIDTH;
    }
    else
    { // Add one character to the buffer
      lcdcounter++;
      // Fill rest of the row with spaces
      for(i=lcdcounter;i<LCD_WIDTH;i++)
        lcdBuffer[i] = ' ';
    }
    lcdBuffer[lcdcounter-1] = text;
  }else{ // Backspace
    if(lcdcounter > 0)
      lcdcounter--;
    lcdBuffer[lcdcounter] = ' ';
  }
  // Set cursor to start of second row
  lcd.setCursor(0,1);
  lcd.print(lcdBuffer);
}

void LCD_::printHEX(unsigned char hex)
{
  lcd.print(hex, HEX);
}

void LCD_::init()
{
  lcd.begin(LCD_WIDTH,LCD_HEIGHT);
  lcd.clear();
}

#endif
