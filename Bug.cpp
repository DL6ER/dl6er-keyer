#include "routines.h"

// Bug Keying
/*************************************************
K1EL says:
 Bug mode directly keys with the dah paddle and
 generates dits automatically when the dit paddle
 is pressed.
*************************************************/
void Bug()
{
  if(state == NONE && dah)
  {
LABEL_DAH:

    if(config)
      key_trx = FALSE;
    else
      key_trx = TRUE;

    if(!pttActive && !TRXMute && key_trx)
    {
      ptt_on();
      if(pttLeadInTime)
      { // LeadInTime is given in units of 10msec
        delay(10*pttLeadInTime);
        // Do this in order to avoid automatic droping of PTT if LeadIn time was high
        keyOffTime = micros();
        // Return so we can check if dah is still pressed in the next round
        return;
      }
    }

    tx();
    if(!config || configFlag == recMem || configFlag == recCall)
      keyedsomething = TRUE;
    time = micros();
    state = DAH;
  }
  else if(state == DAH && !dah)
  {
    rx();
	// Lets assue the user sent a DAH if the tone was longer than 2/3 of the nominal dah-length
    if((micros()-time) >= 2*ditlength)
      ditdahbuffer[ditdahcounter++]='T';
    time = micros();
    state = DAHPAUSE;
  }
  else if(state == NONE && dit)
  {
LABEL_DIT:

   if(config)
      key_trx = FALSE;
    else
      key_trx = TRUE;

    if(!pttActive && !TRXMute && key_trx)
    {
      ptt_on();
      if(pttLeadInTime)
      { // LeadInTime is given in units of 10msec
        delay(10*pttLeadInTime);
        // Do this in order to avoid automatic droping of PTT if LeadIn time was high
        keyOffTime = micros();
        // Return so we can check if dit is still pressed in the next round
        return;
      }
    }

    tx();
    if(!config || configFlag == recMem || configFlag == recCall)
      keyedsomething = TRUE;
    // Add dit to ditdahbuffer for later character evaluation
    ditdahbuffer[ditdahcounter++]='E';
    time = micros();
    // Remember that we are currently sending DIT
    state = DIT;
  }
  else if(state == DIT)
  { // Send DIT
    if((micros()-time) >= ditlength)
    {
      rx();
      time = micros();
      // Now we have to wait a little bit before keying again
      state = DITPAUSE;
    }
  }
  else if(state == DAHPAUSE || state == DITPAUSE)
  {  // Wait after sending a dah
    if((micros()-time) >= ditlength)
    {
      if(dah)
      {
        goto LABEL_DAH;
      }
      else if(dit)
      {
        goto LABEL_DIT;
      }
      else if((micros()-time) >= toleranceTime)
      {
        state = EVALUATECHAR;
       //state = NONE;
      }
    }
  }
}
