year=$(date --utc +'%y')
month=$(date --utc +'%m')
day=$(date --utc +'%d')
hour=$(date --utc +'%H')
minute=$(date --utc +'%M')

echo "// Date of last commit" > date.h
echo "#define commit_hour   $hour" >> date.h
echo "#define commit_minute $minute" >> date.h
echo "#define commit_day    $day" >> date.h
echo "#define commit_month  $month" >> date.h
echo "#define commit_year   $year" >> date.h

echo "#define commit_date   \"$year $month $day / $hour $minute\"" >> date.h
